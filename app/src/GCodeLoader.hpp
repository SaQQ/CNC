// Created by Paweł Sobótka on 24.08.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_GCODE_LOADER_HPP
#define CNC_SIMULATOR_GCODE_LOADER_HPP

#include <memory>
#include <optional>
#include <vector>
#include <string>
#include <stdexcept>

#include <vector.hpp>

#include "PathEvaluatorToolController.hpp"

namespace App {

    class GCodeParseException : public std::runtime_error {
    public:
        explicit GCodeParseException(const std::string &__arg);
    };

    class GCodeCommand {
    public:
        virtual void execute(IToolController *tc) const = 0;
    };

    class G00Command final : public GCodeCommand {
    public:
        using coordType = std::optional<float>;

        G00Command(coordType X, coordType Y, coordType Z);

        void execute(IToolController *tc) const override;

        const coordType X, Y, Z;
    };

    class G01Command final : public GCodeCommand {
    public:
        using coordType = std::optional<float>;

        G01Command(coordType X, coordType Y, coordType Z);

        void execute(IToolController *tc) const override;

        const coordType X, Y, Z;
    };

    using GCodeProgram = std::vector<std::unique_ptr<GCodeCommand>>;

    class GCodeLoader {
    public:
        static std::unique_ptr<GCodeCommand> parseLine(const std::string &code);
        static GCodeProgram loadFromFile(const std::string &path);
    };

}

#endif //CNC_SIMULATOR_GCODE_LOADER_HPP
