// Created by Paweł Sobótka on 30.08.17.
// Rail-Mil Computers Sp. z o.o.

#include "ToolsetModel.hpp"

using namespace maths;

namespace App {

    ToolsetModel::ToolsetModel(QObject *parent)
            : QAbstractListModel{parent},
              sceneRoot{nullptr},
              locked{false} {

    }

    QHash<int, QByteArray> ToolsetModel::roleNames() const {
        auto qt_roles = QAbstractItemModel::roleNames();
        qt_roles[TypeRole] = "type";
        qt_roles[DiameterRole] = "diameter";
        qt_roles[CutLengthRole] = "cutLength";
        qt_roles[LengthRole] = "length";
        return qt_roles;
    }

    int ToolsetModel::rowCount(const QModelIndex &parent) const {
        return static_cast<int>(tools.size());
    }

    Qt::ItemFlags ToolsetModel::flags(const QModelIndex &index) const {
        return QAbstractListModel::flags(index) | Qt::ItemIsEditable;
    }

    QVariant ToolsetModel::data(const QModelIndex &index, int role) const {
        if(!index.isValid())
            return QVariant{};

        auto tool = tools[index.row()];

        switch(role) {
            case TypeRole:
                return QVariant{QString::fromStdString(tool->typeName())};
            case DiameterRole:
                return QVariant{tool->getDiameter()};
            case CutLengthRole:
                return QVariant{tool->getCutLength()};
            case LengthRole:
                return QVariant{tool->getLength()};
            default:
                return QVariant{};
        }
    }

    void ToolsetModel::setSceneRoot(Qt3DCore::QEntity *sceneRoot) {
        this->sceneRoot = sceneRoot;
        for(auto tool : tools)
            tool->setParent(sceneRoot);
        emit sceneRootChanged();
    }

    bool ToolsetModel::setData(const QModelIndex &index, const QVariant &value, int role) {
        if(!index.isValid())
            return false;

        if(locked)
            return false;

        auto tool = tools[index.row()];

        bool modified = false;

        switch(role) {
            case TypeRole:
                if(tool->typeName() != value.toString().toStdString()) {
                    tool->setParent(static_cast<Qt3DCore::QNode*>(nullptr));
                    if(value.toString().toStdString() == FlatTipTool::TypeNameString) {
                        tools[index.row()] = new FlatTipTool{nullptr};
                    } else if (value.toString().toStdString() == SphericalTipTool::TypeNameString) {
                        tools[index.row()] = new SphericalTipTool{nullptr};
                    } else {
                        tools[index.row()] = tool;
                        tools[index.row()]->setParent(sceneRoot);
                        tools[index.row()]->setEnabled(false);
                        return false;
                    }
                    tools[index.row()]->setParent(sceneRoot);
                    tools[index.row()]->setDiameter(tool->getDiameter());
                    tools[index.row()]->setCutLength(tool->getCutLength());
                    tools[index.row()]->setLength(tool->getLength());
                    tools[index.row()]->setPosition(tool->getPosition());
                    tools[index.row()]->setEnabled(false);
                    emit toolReplaced(tool, tools[index.row()]);
                    delete tool;
                    modified = true;
                }
                break;
            case DiameterRole:
                if(tool->getDiameter() != value.toUInt()) {
                    tool->setDiameter(value.toUInt());
                    modified = true;
                }
                break;
            case CutLengthRole:
                if(tool->getCutLength() != value.toUInt()) {
                    tool->setCutLength(value.toUInt());
                    modified = true;
                }
                break;
            case LengthRole:
                if(tool->getLength() != value.toUInt()) {
                    tool->setLength(value.toUInt());
                    modified = true;
                }
                break;
            default:
                return false;
        }

        if(modified)
            emit dataChanged(this->index(index.row(), 0), this->index(index.row()), {role});
        return true;
    }

    bool ToolsetModel::removeRows(int row, int count, const QModelIndex &parent) {
        if(locked)
            return false;
        beginRemoveRows(QModelIndex{}, row, row + count - 1);
        auto c = count;
        while(c--) {
            auto tool = tools[row + c];
            emit toolRemoved(tool);
            tools[row + c] = nullptr;
            tool->setParent(static_cast<Qt3DCore::QNode*>(nullptr));
            delete tool;
        }
        tools.erase(tools.begin() + row, tools.begin() + row + count);
        endRemoveRows();
        return true;
    }

    bool ToolsetModel::insertRows(int row, int count, const QModelIndex &parent) {
        if(locked)
            return false;
        beginInsertRows(QModelIndex{}, row, row + count - 1);
        auto c = count;
        while(c--) {
            Tool* tool = new SphericalTipTool{sceneRoot};
            tool->setEnabled(false);
            tool->setPosition(QVector3D{0.0f, MachineConstants::WorkspaceMaxY, 0.0f});
            emit toolAdded(tool);
            tools.insert(tools.begin() + row + c, tool);
        }
        endInsertRows();
        return true;
    }

    bool ToolsetModel::addTool(const QString &type, unsigned int diameter) {

        Tool* tool = nullptr;

        if (type == SphericalTipTool::TypeNameString) {
            tool = new SphericalTipTool{sceneRoot};
        } else if (type == FlatTipTool::TypeNameString) {
            tool = new FlatTipTool{sceneRoot};
        } else {
            return false;
        }

        tool->setEnabled(false);
        tool->setPosition(QVector3D{0.0f, MachineConstants::WorkspaceMaxY, 0.0f});
        tool->setDiameter(diameter);

        beginInsertRows(QModelIndex{}, static_cast<int>(tools.size()), static_cast<int>(tools.size()));
        tools.push_back(tool);
        endInsertRows();

        emit toolAdded(tool);
    }

    int ToolsetModel::findIndex(Tool *tool) const {
        auto tool_it = std::find(tools.begin(), tools.end(), tool);
        if(tool_it !=  tools.end())
            return static_cast<int>(std::distance(tools.begin(), tool_it));
        else return -1;
    }

    Tool* ToolsetModel::findTool(const QString &type, unsigned int diameter) {
        for (auto tool : tools) {
            if (tool->getDiameter() == diameter && tool->getType() == type)
                return tool;
        }
        return nullptr;
    }

    QVariant ToolsetModel::getTool(int index) {
        if (index < 0 || index >= static_cast<int>(tools.size()))
            return QVariant::fromValue<Tool*>(nullptr);
        return QVariant::fromValue<Tool*>(tools[index]);
    }


}