// Created by Paweł Sobótka on 05.09.17.
// Rail-Mil Computers Sp. z o.o.

#include "RunProgramOperationSequence.hpp"

namespace App
{

    RunProgramOperationSequence::RunProgramOperationSequence(const ProgramModel *program)
            : stage{Stage::NEXT_STEP_INIT}, program{program},
              currentStepIndex{-1}, currentStep{nullptr}, prevStepsProgress{0.0f}, toEnd{false}
    {

    }

    bool RunProgramOperationSequence::tick(IMillable *material, ISimulationController *sim, qint64 elapsed)
    {
        qint64 unusedTime = elapsed;
        do
        {
            switch (stage)
            {
                case Stage::NEXT_STEP_INIT:
                    currentStepIndex++;
                    if (currentStepIndex < program->stepCount())
                    {
                        currentStep = &program->getStep(currentStepIndex);
                        auto stepTool = program->getTool(currentStepIndex);

                        if (sim != nullptr)
                        {
                            if (sim->getCurrentTool() != stepTool)
                                sim->setCurrentTool(stepTool);
                            sim->getCurrentTool()->setPosition(QVector3D{0.0f, MachineConstants::WorkspaceMaxY, 0.0f});
                        }

                        toolController = std::make_unique<ToolController>(maths::vec3{{0.0f, MachineConstants::WorkspaceMaxY, 0.0f}});

                        currentStepIterator = currentStep->begin();
                        (*currentStepIterator)->execute(toolController.get());
                        stage = Stage::RUNNING_COMMAND;
                    }
                    else
                    {
                        if (sim != nullptr)
                        {
                            sim->setCurrentTool(nullptr);
                        }
                        stage = Stage::FINISHED;
                    }
                    break;
                case Stage::RUNNING_COMMAND:
                {
                    auto stepTool = program->getTool(currentStepIndex);

                    bool moveFinished;
                    auto previousPosition = toolController->getPosition();
                    std::tie(moveFinished, unusedTime) = toolController->update(elapsed, toEnd);
                    auto position = toolController->getPosition();

                    if (sim != nullptr) {
                        sim->getCurrentTool()->setPosition(QVector3D(position.x(), position.y(), position.z()));
                    }

                    if (stepTool->getType() == "flat") {
                        material->millFlat(material->toLocalSpace(previousPosition), material->toLocalSpace(position),
                                           stepTool->getDiameter() / 2.0f, sim != nullptr);
                    } else if (stepTool->getType() == "spherical") {
                        material->millSpherical(material->toLocalSpace(previousPosition), material->toLocalSpace(position),
                                                stepTool->getDiameter() / 2.0f, sim != nullptr);
                    }

                    if (moveFinished)
                    {
                        currentStepIterator++;
                        if (currentStepIterator == currentStep->end())
                        {
                            prevStepsProgress += toolController->getTotalDistance();
                            stage = Stage::NEXT_STEP_INIT;
                        } else
                        {
                            (*currentStepIterator)->execute(toolController.get());
                        }
                    }
                    break;
                }
                case Stage::FINISHED:
                    break;
            }
        } while (unusedTime > 0 && toEnd && !finished());
        return finished();
    }

    bool RunProgramOperationSequence::finished()
    {
        return stage == Stage::FINISHED;
    }

    float RunProgramOperationSequence::progress()
    {
        switch (stage)
        {
            case Stage::NEXT_STEP_INIT:
            case Stage::FINISHED:
                return prevStepsProgress;
            case Stage::RUNNING_COMMAND:
                return prevStepsProgress + toolController->getTotalDistance();
        }
        return 0.0f;
    }

    void RunProgramOperationSequence::runToEnd()
    {
        toEnd = true;
    }

}