// Created by Paweł Sobótka on 05.09.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_ISIMULATIONCONTROLLER_HPP
#define CNC_SIMULATOR_ISIMULATIONCONTROLLER_HPP

#include "Tool.hpp"

namespace App {

    class ISimulationController {
    public:
        virtual Tool* getCurrentTool() const = 0;
        virtual void setCurrentTool(Tool* tool) = 0;
    };

}

#endif //CNC_SIMULATOR_ISIMULATIONCONTROLLER_HPP
