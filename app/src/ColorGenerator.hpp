// Created by Paweł Sobótka on 27.10.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_COLORGENERATOR_HPP
#define CNC_SIMULATOR_COLORGENERATOR_HPP

#include <array>

#include <QObject>
#include <QColor>

namespace App {

    class ColorGenerator : public QObject {
        Q_OBJECT
    public:
        explicit ColorGenerator(QObject *parent = nullptr);

        Q_INVOKABLE QColor getColor(int index);

    private:
        std::array<QColor, 6> colors;

    };

}

#endif //CNC_SIMULATOR_COLORGENERATOR_HPP
