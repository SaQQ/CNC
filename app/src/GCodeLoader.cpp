// Created by Paweł Sobótka on 24.08.17.
// Rail-Mil Computers Sp. z o.o.

#include "GCodeLoader.hpp"

#include <map>
#include <fstream>

namespace App {

    GCodeParseException::GCodeParseException(const std::string &__arg) : std::runtime_error(__arg) {}

    using integralArgsType = std::map<char, int>;
    using floatArgsType = std::map<char, float>;

    std::unique_ptr<GCodeCommand> constructCommand(const integralArgsType &integralArgs, const floatArgsType &floatArgs) {
        if(!integralArgs.count('G'))
            throw GCodeParseException{"No G key in command"};
        switch(integralArgs.at('G')) {
            case 0:
            {
                std::optional<float> x, y, z;
                if(floatArgs.count('X')) x = floatArgs.at('X');
                if(floatArgs.count('Y')) y = floatArgs.at('Y');
                if(floatArgs.count('Z')) z = floatArgs.at('Z');
                return std::make_unique<G00Command>(x, z, y);
            }
            case 1:
            {
                std::optional<float> x, y, z;
                if(floatArgs.count('X')) x = floatArgs.at('X');
                if(floatArgs.count('Y')) y = floatArgs.at('Y');
                if(floatArgs.count('Z')) z = floatArgs.at('Z');
                return std::make_unique<G01Command>(x, z, y);
            }
            default:
                throw GCodeParseException{"Unrecognized G-Code"};
        }
    }

    std::unique_ptr<GCodeCommand> GCodeLoader::parseLine(const std::string &code) {

        std::size_t i = 0;

        integralArgsType integralArgs;
        floatArgsType floatArgs;

        while(i < code.length()) {
            auto right = code.substr(i + 1);
            std::size_t parsed;
            switch(code.at(i)) {
                case 'N':
                case 'G':
                case 'S':
                case 'M':
                case 'F':
                    if(integralArgs.count(code.at(i)) > 0)
                        throw GCodeParseException{"Repeated key in command"};
                    integralArgs[code.at(i)] = std::stoi(right, & parsed);
                    break;
                case 'X':
                case 'Y':
                case 'Z':
                    if(floatArgs.count(code.at(i)) > 0)
                        throw GCodeParseException{"Repeated key in command"};
                    floatArgs[code.at(i)] = std::stof(right, &parsed);
                    break;
                default:
                    throw GCodeParseException{"Unrecognized key in command"};
            }
            if(parsed == 0)
                throw GCodeParseException{"Empty key"};
            i += (1 + parsed);
        }

        return constructCommand(integralArgs, floatArgs);

    }

    GCodeProgram GCodeLoader::loadFromFile(const std::string &path) {
        GCodeProgram program;

        std::ifstream file;

        file.exceptions(std::ios_base::badbit);

        file.open(path);

        std::string line;

        while(file >> line) {
            if(line.size() == 0)
                continue;
            program.push_back(parseLine(line));
        }

        return program;
    }

    G00Command::G00Command(G00Command::coordType X, G00Command::coordType Y, G00Command::coordType Z) : X{X}, Y{Y}, Z{Z} {

    }

    void G00Command::execute(IToolController *tc) const {
        tc->rapidPosition(X, Y, Z);
    }

    G01Command::G01Command(G01Command::coordType X, G01Command::coordType Y, G01Command::coordType Z) : X{X}, Y{Y}, Z{Z} {

    }

    void G01Command::execute(IToolController *tc) const {
        tc->linearInterpolate(X, Y, Z);
    }


}
