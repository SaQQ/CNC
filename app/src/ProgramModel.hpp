// Created by Paweł Sobótka on 25.08.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_PROGRAM_SEQUENCE_MODEL_HPP
#define CNC_SIMULATOR_PROGRAM_SEQUENCE_MODEL_HPP

#include <vector>

#include <QUrl>
#include <QAbstractListModel>
#include <QtCore/QVariant>
#include <QModelIndex>

#include "ToolsetModel.hpp"
#include "GCodeLoader.hpp"

namespace App {

    struct ProgramStep {
        Tool* tool = nullptr;
        QString filename;
        GCodeProgram program;
        float pathLength;

        bool valid() const {
            return tool != nullptr;
        }
    };

    class ProgramModel : public QAbstractListModel {
        Q_OBJECT

        Q_PROPERTY(float totalPathLength READ getTotalPathLength NOTIFY totalPathLengthChanged)
        Q_PROPERTY(bool locked READ getLocked WRITE setLocked NOTIFY lockedChanged)
        Q_PROPERTY(bool valid READ getValid NOTIFY validChanged)
        Q_PROPERTY(ToolsetModel* toolset READ getToolset WRITE setToolset NOTIFY toolsetChanged)
    public:
        enum ProgramModelRoles {
            FileNameRole = Qt::UserRole + 1,
            ToolRole,
            PathLengthRole
        };

        explicit ProgramModel();

        float getTotalPathLength() const;

        bool getLocked() const {
            return locked;
        }

        void setLocked(bool value) {
            if(locked != value) {
                locked = value;
                emit lockedChanged();
            }
        }

        bool getValid() const {
            return valid;
        }

        ToolsetModel* getToolset() const {
            return toolset;
        }

        void setToolset(ToolsetModel* toolset) {
            if (this->toolset != nullptr) {
                QObject::disconnect(this->toolset, &ToolsetModel::toolAdded, this, &ProgramModel::toolAdded);
                QObject::disconnect(this->toolset, &ToolsetModel::toolRemoved, this, &ProgramModel::toolRemoved);
                QObject::disconnect(this->toolset, &ToolsetModel::toolReplaced, this, &ProgramModel::toolReplaced);
            }
            this->toolset = toolset;
            if (this->toolset != nullptr) {
                QObject::connect(this->toolset, &ToolsetModel::toolAdded, this, &ProgramModel::toolAdded);
                QObject::connect(this->toolset, &ToolsetModel::toolRemoved, this, &ProgramModel::toolRemoved);
                QObject::connect(this->toolset, &ToolsetModel::toolReplaced, this, &ProgramModel::toolReplaced);
            }
            emit toolsetChanged();
        }

        QHash<int, QByteArray> roleNames() const override;

        int rowCount(const QModelIndex &parent = QModelIndex{}) const override;

        QVariant data(const QModelIndex &index, int role) const override;

        Q_INVOKABLE bool setData(const QModelIndex &index, const QVariant &value, int role) override;

        Q_INVOKABLE bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex{}) override;

        //Q_INVOKABLE bool moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild) override;

        Q_INVOKABLE bool loadStep(const QUrl &path);

        bool pushStep(const QString &filename, GCodeProgram &&program, Tool *tool);

        int stepCount() const {
            return rowCount(QModelIndex{});
        }

        Tool* getTool(int step) const {
            return steps[step].tool;
        }

        const GCodeProgram& getStep(int step) const {
            return steps[step].program;
        }

    signals:
        void totalPathLengthChanged();
        void lockedChanged();
        void validChanged();
        void toolsetChanged();

        void toolRequested(const QString& type, unsigned int diameter);

        void loadError(const QString& message);

    private slots:
        void toolAdded(Tool *tool);
        void toolRemoved(Tool *tool);
        void toolReplaced(Tool *oldTool, Tool *newTool);

    private:
        ToolsetModel* toolset;

        std::vector<ProgramStep> steps;
        float totalPathLength;
        bool locked;
        bool valid;

        void validate();
    };

}

#endif //CNC_SIMULATOR_PROGRAM_SEQUENCE_MODEL_HPP
