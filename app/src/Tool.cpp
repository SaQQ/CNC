// Created by Paweł Sobótka on 25.08.17.
// Rail-Mil Computers Sp. z o.o.

#include "Tool.hpp"

namespace App {

    Tool::Tool(Qt3DCore::QEntity *parent)
            : Qt3DCore::QEntity{parent},
              diameter{DefaultDiameter},
              cut_length{DefaultCutLength},
              length{DefaultLength} {

        transform = new Qt3DCore::QTransform{this};

        this->addComponent(transform);

        tip_material = new Qt3DExtras::QGoochMaterial{this};

        tip_material->setWarm(QColor{255, 202, 45});
        tip_material->setCool(QColor{121, 46, 201});

        shaft_entity = new Qt3DCore::QEntity{this};
        shaft_mesh = new Qt3DExtras::QCylinderMesh{this};
        shaft_transform = new Qt3DCore::QTransform{this};
        shaft_material = new Qt3DExtras::QGoochMaterial{this};

        shaft_material->setWarm(QColor{204, 204, 204});
        shaft_material->setCool(QColor{107, 107, 107});

        shaft_entity->addComponent(shaft_mesh);
        shaft_entity->addComponent(shaft_transform);
        shaft_entity->addComponent(shaft_material);

        updateShaft();
    }

    void Tool::updateShaft() {
        shaft_mesh->setLength(length - cut_length);
        shaft_mesh->setRadius(diameter / 2.0f);
        shaft_transform->setTranslation(QVector3D{0.0f, (length - cut_length) / 2.0f + cut_length, 0.0f});
    }

    void Tool::updateTip() {

    }


}