// Created by Paweł Sobótka on 29.08.17.
// Rail-Mil Computers Sp. z o.o.

#include "FlatTipTool.hpp"

using namespace maths;

namespace App {

    FlatTipTool::FlatTipTool(Qt3DCore::QEntity *parent) : Tool(parent) {
        tip_entity = new Qt3DCore::QEntity{this};
        tip_mesh = new Qt3DExtras::QCylinderMesh{this};
        tip_transform = new Qt3DCore::QTransform{this};

        tip_entity->addComponent(tip_mesh);
        tip_entity->addComponent(tip_transform);
        tip_entity->addComponent(tip_material);

        updateTip();
    }

    void FlatTipTool::updateTip() {
        tip_mesh->setLength(cut_length);
        tip_mesh->setRadius(diameter / 2.0f);
        tip_transform->setTranslation(QVector3D{0.0f, cut_length / 2.0f, 0.0f});
    }

    float FlatTipTool::intersectionDepth(const maths::vec3 &point) const {
        auto axis_distance = (vec2{{point.x(), point.z()}}).length();

        bool in_cyllinder = axis_distance <= diameter / 2.0f;
        bool in_slice = point.y() >= 0.0f && point.y() <= cut_length;

        if(in_cyllinder && in_slice) {
            return std::min(std::min(point.y(), cut_length - point.y()), diameter / 2.0f - axis_distance);
        } else {
            return -1.0f;
        }
    }

    std::string FlatTipTool::typeName() const {
        return TypeNameString;
    }
}