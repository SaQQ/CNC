// Created by Paweł Sobótka on 29.08.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_SPHERICALTIPTOOL_HPP
#define CNC_SIMULATOR_SPHERICALTIPTOOL_HPP

#include "Tool.hpp"

#include <Qt3DExtras/QSphereMesh>

namespace App {

    class SphericalTipTool : public Tool {
        Q_OBJECT
    public:
        constexpr static auto TypeNameString = "spherical";

        explicit SphericalTipTool(Qt3DCore::QEntity *parent = nullptr);

        std::string typeName() const override;

        float intersectionDepth(const maths::vec3 &point) const override;

    private:
        Qt3DCore::QEntity* tip_cylinder_entity;
        Qt3DExtras::QCylinderMesh* tip_cylinder_mesh;
        Qt3DCore::QTransform* tip_cylinder_transform;

        Qt3DCore::QEntity* tip_sphere_entity;
        Qt3DExtras::QSphereMesh* tip_sphere_mesh;
        Qt3DCore::QTransform* tip_sphere_transform;

        void updateTip() override;
    };

}

#endif //CNC_SIMULATOR_SPHERICALTIPTOOL_HPP
