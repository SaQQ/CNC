// Created by Paweł Sobótka on 04.09.17.
// Rail-Mil Computers Sp. z o.o.

#include "PathEvaluatorToolController.hpp"

#include "MachineConstants.hpp"

using namespace maths;

namespace App {

    PathEvaluatorToolController::PathEvaluatorToolController()
            : lastPosition{{0.0f, MachineConstants::WorkspaceMaxY, 0.0f}}, totalDistance{0.0f} {

    }

    void PathEvaluatorToolController::rapidPosition(optFloat x, optFloat y, optFloat z) {
        moveTo(x, y, z);
    }

    void PathEvaluatorToolController::linearInterpolate(optFloat x, optFloat y, optFloat z) {
        moveTo(x, y, z);
    }

    void PathEvaluatorToolController::moveTo(optFloat x, optFloat y, optFloat z) {
        vec3 newPosition{{
                                 x.has_value() ? x.value() : lastPosition.x(),
                                 y.has_value() ? y.value() : lastPosition.y(),
                                 z.has_value() ? z.value() : lastPosition.z()
                         }};

        totalDistance += (newPosition - lastPosition).length();

        lastPosition = newPosition;
    }

}