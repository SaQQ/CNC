// Created by Paweł Sobótka on 29.08.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_FLATTIPTOOL_HPP
#define CNC_SIMULATOR_FLATTIPTOOL_HPP

#include "Tool.hpp"

namespace App {

    class FlatTipTool : public Tool {
        Q_OBJECT
    public:
        constexpr static auto TypeNameString = "flat";

        explicit FlatTipTool(Qt3DCore::QEntity *parent = nullptr);

        std::string typeName() const override;

        float intersectionDepth(const maths::vec3 &point) const override;

    private:
        Qt3DCore::QEntity* tip_entity;
        Qt3DExtras::QCylinderMesh* tip_mesh;
        Qt3DCore::QTransform* tip_transform;

        void updateTip() override;
    };

}

#endif //CNC_SIMULATOR_FLATTIPTOOL_HPP
