// Created by Paweł Sobótka on 25.08.17.
// Rail-Mil Computers Sp. z o.o.

#include "ProgramModel.hpp"

namespace App {

    ProgramModel::ProgramModel()
            : toolset{nullptr},
              totalPathLength{0.0f},
              locked{false},
              valid{false} {

    }

    int ProgramModel::rowCount(const QModelIndex &parent) const {
        return static_cast<int>(steps.size());
    }

    QVariant ProgramModel::data(const QModelIndex &index, int role) const {
        if(!index.isValid())
            return QVariant{};

        const auto& step = steps[index.row()];

        switch(role) {
            case FileNameRole:
                return QVariant{step.filename};
            case ToolRole:
                return QVariant::fromValue<QObject*>(step.tool);
            case PathLengthRole:
                return QVariant{step.pathLength};
            default:
                return QVariant{};
        }
    }

    bool ProgramModel::setData(const QModelIndex &index, const QVariant &value, int role) {
        if(!index.isValid())
            return false;

        if(locked)
            return false;

        auto& step = steps[index.row()];

        switch(role) {
            case ToolRole:
            {
                auto newTool = qvariant_cast<Tool*>(value);
                if(step.tool != newTool) {
                    step.tool = newTool;
                    emit dataChanged(this->index(index.row()), this->index(index.row()), {role});
                    validate();
                }
                return true;
            }
            default:
                return false;
        }
    }

    bool ProgramModel::removeRows(int row, int count, const QModelIndex &parent) {
        if(locked)
            return false;
        beginRemoveRows(QModelIndex{}, row, row + count - 1);
        steps.erase(steps.begin() + row, steps.begin() + row + count);
        endRemoveRows();
        emit totalPathLengthChanged();
        validate();
        return true;
    }

    QHash<int, QByteArray> ProgramModel::roleNames() const {
        auto qt_roles = QAbstractItemModel::roleNames();
        qt_roles[FileNameRole] = "fileName";
        qt_roles[ToolRole] = "tool";
        qt_roles[PathLengthRole] = "pathLength";
        return qt_roles;
    }

    bool ProgramModel::pushStep(const QString &filename, GCodeProgram &&program, Tool *tool) {
        if(locked)
            return false;

        ProgramStep newStep;

        newStep.filename = filename;
        newStep.program = std::move(program);
        newStep.tool = tool;

        PathEvaluatorToolController evaluator;

        for(const auto& command: newStep.program) {
            command->execute(&evaluator);
        }

        newStep.pathLength = evaluator.getTotalDistance();

        beginInsertRows(QModelIndex{}, static_cast<int>(steps.size()), static_cast<int>(steps.size()));

        steps.push_back(std::move(newStep));

        endInsertRows();

        emit totalPathLengthChanged();

        validate();

        return true;
    }


    std::pair<QString, unsigned int> parseExtension(const std::string& ext) {
        if (ext.length() < 2)
            throw std::invalid_argument{"Too short extension"};

        QString type;

        switch(ext[0]) {
            case 'k':
                type = "spherical";
                break;
            case 'f':
                type = "flat";
                break;
            default:
                throw std::invalid_argument{"Unknown tool type"};
        }

        auto diameter = static_cast<unsigned int>(std::atoi(ext.substr(1).data()));

        return {type, diameter};
    }

    bool ProgramModel::loadStep(const QUrl &path) {
        try {
            auto program = GCodeLoader::loadFromFile(path.toLocalFile().toStdString());
            auto extension = path.toLocalFile().split(".").last().toStdString();

            Tool* tool = nullptr;

            if (toolset != nullptr) {
                QString type;
                unsigned int diameter = 0;

                try {
                    std::tie(type, diameter) = parseExtension(extension);
                    tool = toolset->findTool(type, diameter);

                    if (tool == nullptr)
                            emit toolRequested(type, diameter);

                    tool = toolset->findTool(type, diameter);
                } catch(const std::invalid_argument &e) {

                }

            }

            pushStep(path.fileName(), std::move(program), tool);

            return true;
        } catch(const GCodeParseException &ex) {
            emit loadError(ex.what());
            return false;
        }
    }

    void ProgramModel::validate() {
        auto newValue = !steps.empty() && std::all_of(steps.begin(), steps.end(),
                                                      [](const ProgramStep &step) { return step.valid(); });
        if(valid != newValue) {
            valid = newValue;
            emit validChanged();
        }
    }

    void ProgramModel::toolAdded(Tool *tool) {

    }

    void ProgramModel::toolRemoved(Tool *tool) {
        for(std::size_t i = 0; i < steps.size(); ++i) {
            if (steps[i].tool == tool)
                steps[i].tool = nullptr;
            emit dataChanged(this->index(static_cast<int>(i)), this->index(static_cast<int>(i)), {ToolRole});
        }
    }

    void ProgramModel::toolReplaced(Tool *oldTool, Tool *newTool) {
        for(std::size_t i = 0; i < steps.size(); ++i) {
            if (steps[i].tool == oldTool)
                steps[i].tool = newTool;
            emit dataChanged(this->index(static_cast<int>(i)), this->index(static_cast<int>(i)), {ToolRole});
        }
    }

    float ProgramModel::getTotalPathLength() const {
        return std::accumulate(steps.begin(), steps.end(), 0.0f, [](float acc, const ProgramStep& step) { return acc + step.pathLength; });
    }

//    bool ProgramModel::moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild) {
//        if(count < 0)
//            return false;
//
//        beginMoveRows(QModelIndex{}, sourceRow, sourceRow + count - 1, QModelIndex{}, destinationChild);
//
//        std::vector<ProgramStep> movedSteps;
//        movedSteps.resize(count);
//
//        std::copy(steps.begin() + sourceRow, steps.begin() + sourceRow + count, movedSteps.begin());
//        steps.erase(steps.begin() + sourceRow, steps.begin() + sourceRow + count);
//        steps.insert(steps)
//
//        endMoveRows();
//
//        return false;
//    }

}