#ifndef CNC_SIMULATOR_IMILLABLE_HPP
#define CNC_SIMULATOR_IMILLABLE_HPP

namespace App {

    class IMillable {
    public:

        class MillingError : public std::runtime_error {
        public:
            explicit MillingError(const std::string &message) : runtime_error(message) {}
        };

        virtual ~IMillable() = default;

        virtual maths::vec3 toLocalSpace(const maths::vec3 &v) = 0;

        virtual void millSpherical(const maths::vec3 &startPos, const maths::vec3 &endPos,
                                   float radius, bool updateBuffers) = 0;

        virtual void millFlat(const maths::vec3 &startPos, const maths::vec3 &endPos,
                              float radius, bool updateBuffers) = 0;

        virtual void updateAllNormals() = 0;

        virtual void fullBufferUpdate() = 0;

    };

}

#endif //CNC_SIMULATOR_IMILLABLE_HPP
