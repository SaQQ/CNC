#ifndef CNC_SIMULATOR_MATERIALCHUNK_HPP
#define CNC_SIMULATOR_MATERIALCHUNK_HPP

#include <vector>
#include <functional>

#include <Qt3DCore/QEntity>
#include <Qt3DCore/QTransform>

#include <Qt3DRender/QBuffer>
#include <Qt3DRender/QAttribute>

#include <vector.hpp>

#include "IMillable.hpp"

namespace App {

    class MaterialChunk : public Qt3DCore::QEntity, public IMillable {
        Q_OBJECT

        Q_PROPERTY(unsigned int xCells READ getXCells NOTIFY xCellsChanged)
        Q_PROPERTY(unsigned int zCells READ getZCells NOTIFY zCellsChanged)
        Q_PROPERTY(float xExtent READ getXExtent NOTIFY xExtentChanged)
        Q_PROPERTY(float zExtent READ getZExtent NOTIFY zExtentChanged)
        Q_PROPERTY(float height READ getHeight NOTIFY heightChanged)
        Q_PROPERTY(float heightThreshold READ getHeightThreshold NOTIFY heightThresholdChanged)

        struct vertex {
            maths::vec3 pos;
            maths::vec3 normal;
            maths::vec2 texCoord;
        };
    public:
        constexpr static unsigned int DefaultXCells = 300;
        constexpr static unsigned int DefaultZCells = 300;
        constexpr static float DefaultXExtent = 150.0f;
        constexpr static float DefaultZExtent = 150.0f;
        constexpr static float DefaultHeight = 50.0f;
        constexpr static float DefaultHeightThreshold = 20.0f;

        explicit MaterialChunk(Qt3DCore::QEntity *parent = nullptr);

        unsigned int getXCells() const {
            return xCells;
        }

        unsigned int getZCells() const {
            return zCells;
        }

        float getXExtent() const {
            return xExtent;
        }

        float getZExtent() const {
            return zExtent;
        }

        float getHeight() const {
            return height;
        }

        float getHeightThreshold() const {
            return heightThreshold;
        }

        Q_INVOKABLE void reset();

        Q_INVOKABLE void regenerate(float xExtent, float zExtent, float height, unsigned int xCells, unsigned int zCells, float heightThreshold);

        maths::vec3 toLocalSpace(const maths::vec3 &v) override;

        void millSpherical(const maths::vec3 &startPos, const maths::vec3 &endPos,
                           float radius, bool updateBuffers) override;

        void millFlat(const maths::vec3 &startPos, const maths::vec3 &endPos,
                      float radius, bool updateBuffers) override;

        void updateAllNormals() override;

        Q_INVOKABLE void fullBufferUpdate() override;

    signals:
        void xCellsChanged();
        void zCellsChanged();
        void xExtentChanged();
        void zExtentChanged();
        void heightChanged();
        void heightThresholdChanged();

    private:
        using machineStripHeightFunction = std::function<float(float, float)>;
        using fillFunction = std::function<void(int, int)>;
        using lineProcessedCallback = std::function<void(int, int, int)>;
        using processGrainFunction = std::function<void(std::size_t, std::size_t)>;
        using gainsLineProcessedCallback = std::function<void(std::size_t, std::size_t, std::size_t)>;

        unsigned int xCells, zCells;
        float xExtent, zExtent;
        float height;
        float heightThreshold;

        std::vector<vertex> vertexBuffer;
        std::vector<unsigned int> indexBuffer;

        Qt3DRender::QBuffer* vbo;
        Qt3DRender::QBuffer* ibo;

        Qt3DRender::QAttribute* positionAttr;
        Qt3DRender::QAttribute* normalAttr;
        Qt3DRender::QAttribute* texCoordAttr;
        Qt3DRender::QAttribute* indexAttr;

        Qt3DCore::QTransform* transform;

        vertex* surfaceVertices;

        vertex* negativeXG;
        vertex* positiveXG;
        vertex* negativeZG;
        vertex* positiveZG;

        vertex* negativeXD;
        vertex* positiveXD;
        vertex* negativeZD;
        vertex* positiveZD;

        void setXCells(unsigned int cells) {
            if (xCells != cells) {
                xCells = cells;
                emit xCellsChanged();
            }
        }

        void setZCells(unsigned int cells) {
            if (zCells != cells) {
                zCells = cells;
                emit zCellsChanged();
            }
        }

        void setXExtent(float extent) {
            xExtent = extent;
            emit xExtentChanged();
        }

        void setZExtent(float extent) {
            zExtent = extent;
            emit zExtentChanged();
        }

        void setHeight(float height) {
            this->height = height;
            emit heightChanged();
        }

        void setHeightThreshold(float threshold) {
            this->heightThreshold = threshold;
            emit heightThresholdChanged();
        }

        void setHeight(std::size_t x, std::size_t z, float height);

        void updateNormal(std::size_t x, std::size_t z);

        bool millGrain(std::size_t x, std::size_t z, machineStripHeightFunction heightFunction);

        void processGrains(const maths::vec3 &startPos, const maths::vec3 &endPos, float radius, processGrainFunction process, gainsLineProcessedCallback lineProcessed);

        void processRectangle(const maths::vec2 &v1, const maths::vec2 &v2, const maths::vec2 &v3, const maths::vec2 &v4, fillFunction fill, lineProcessedCallback lineProcessed);

        size_t mill(const maths::vec3 &startPos, const maths::vec3 &endPos, float radius,
                    MaterialChunk::machineStripHeightFunction heightFunction, bool updateBuffers = true);
    };

}

#endif //CNC_SIMULATOR_MATERIALCHUNK_HPP
