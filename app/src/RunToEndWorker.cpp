// Created by Paweł Sobótka on 27.10.17.
// Rail-Mil Computers Sp. z o.o.

#include "RunToEndWorker.hpp"

namespace App {

    void RunToEndWorker::run(RunProgramOperationSequence *op, MaterialChunk *material)
    {
        emit progressChanged(0.0f);

        std::unique_ptr<RunProgramOperationSequence> operation{op};

        try
        {
            while (!operation->finished())
            {
                operation->tick(material, nullptr, 200);
                emit progressChanged(operation->progress());
            }
            emit done();
        }
        catch (const MaterialChunk::MillingError& e)
        {
            emit error(e.what());
        }

    }

}