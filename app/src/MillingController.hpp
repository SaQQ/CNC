#ifndef CNC_SIMULATOR_MILLINGCONTROLLER_HPP
#define CNC_SIMULATOR_MILLINGCONTROLLER_HPP

#include <QObject>
#include <QThread>

#include <QTimer>
#include <QElapsedTimer>

#include "MaterialChunk.hpp"
#include "ToolsetModel.hpp"
#include "ProgramModel.hpp"

#include "ISimulationController.hpp"
#include "RunProgramOperationSequence.hpp"

namespace App {

    class MillingController : public QObject, public ISimulationController {
        Q_OBJECT

        Q_PROPERTY(QString state READ getState NOTIFY stateChanged)

        Q_PROPERTY(float progress READ getProgress NOTIFY progressChanged)
        Q_PROPERTY(float speedMultiplier READ getSpeedMultiplier WRITE setSpeedMultiplier NOTIFY speedMultiplierChanged)

        Q_PROPERTY(Tool* currentTool READ getCurrentTool WRITE setCurrentTool NOTIFY currentToolChanged)

        Q_PROPERTY(MaterialChunk* material READ getMaterial WRITE setMaterial NOTIFY materialChanged)
        Q_PROPERTY(ToolsetModel* toolset READ getToolset WRITE setToolset NOTIFY toolsetChanged)
        Q_PROPERTY(ProgramModel* program READ getProgram WRITE setProgram NOTIFY programChanged)

    public:
        constexpr static int updateMsecInterval = 10;

        explicit MillingController(QObject *parent = nullptr);

        ~MillingController() override;

        Q_INVOKABLE bool start();

        Q_INVOKABLE bool reset();

        Q_INVOKABLE bool runToEnd();

        QString getState() const {
            return state;
        }

        float getProgress() {
            return progress;
        }

        float getSpeedMultiplier() const {
            return speedMultiplier;
        }

        void setSpeedMultiplier(float value) {
            speedMultiplier = value;
            emit speedMultiplierChanged();
        }

        Tool* getCurrentTool() const override {
            return currentTool;
        }

        void setCurrentTool(Tool* tool) override;

        MaterialChunk* getMaterial() const {
            return material;
        }

        void setMaterial(MaterialChunk* material) {
            this->material = material;
            emit materialChanged();
        }

        ToolsetModel* getToolset() const {
            return toolset;
        }

        void setToolset(ToolsetModel* toolset) {
            this->toolset = toolset;
            emit toolsetChanged();
        }

        ProgramModel* getProgram() const {
            return program;
        }

        void setProgram(ProgramModel* program) {
            if (this->program != nullptr) {
                disconnect(this->program, &ProgramModel::validChanged, this, &MillingController::programValidChanged);
            }
            this->program = program;
            if (this->program != nullptr) {
                connect(this->program, &ProgramModel::validChanged, this, &MillingController::programValidChanged);
                programValidChanged();
            }
            emit programChanged();
        }

    signals:
        void stateChanged();

        void progressChanged();
        void speedMultiplierChanged();

        void currentToolChanged();

        void materialChanged();
        void toolsetChanged();
        void programChanged();

        void startRunToEndWork(RunProgramOperationSequence* operation, MaterialChunk* material);

        void millingError(const QString& message);

    private slots:
        void tick();

        void programValidChanged();

        void runToEndWorkerProgressChanged(float progress);
        void runToEndWorkerError(const QString& message);
        void runToEndWorkerFinished();

    private:
        QThread workerThread;

        QString state;

        float progress;
        float speedMultiplier;

        Tool* currentTool;

        MaterialChunk* material;
        ToolsetModel* toolset;
        ProgramModel* program;

        QTimer* timer;
        QElapsedTimer* elapsedTimer;

        std::unique_ptr<RunProgramOperationSequence> operation;

        void setState(const QString& state) {
            if (this->state != state) {
                this->state = state;
                emit stateChanged();
            }
        }

        void setProgress(float value) {
            progress = value;
            emit progressChanged();
        }

    };

}

#endif //CNC_SIMULATOR_MILLINGCONTROLLER_HPP
