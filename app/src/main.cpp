// Created by Paweł Sobótka on 18.08.17.
// Rail-Mil Computers Sp. z o.o.

#include <array>

#include <clocale>

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQmlContext>

#include <intersections.hpp>

#include "FlatTipTool.hpp"
#include "SphericalTipTool.hpp"
#include "ToolsetModel.hpp"
#include "MaterialChunk.hpp"
#include "MillingController.hpp"
#include "ToolPath.hpp"

using namespace App;


int main(int argc, char** argv) {

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<Tool>();
    qmlRegisterType<FlatTipTool>("CNCSimulator.Tools", 1, 0, "FlatTipTool");
    qmlRegisterType<SphericalTipTool>("CNCSimulator.Tools", 1, 0, "SphericalTipTool");
    qmlRegisterType<ToolPath>("CNCSimulator.Simulation", 1, 0, "ToolPath");
    qmlRegisterType<MaterialChunk>("CNCSimulator", 1, 0, "MaterialChunk");
    qmlRegisterType<ToolsetModel>("CNCSimulator", 1, 0, "ToolsetModel");
    qmlRegisterType<ProgramModel>("CNCSimulator", 1, 0, "ProgramModel");
    qmlRegisterType<MillingController>("CNCSimulator", 1, 0, "MillingController");

    engine.load(QUrl("qrc:/main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    std::setlocale(LC_NUMERIC, "C");

    return app.exec();
}
