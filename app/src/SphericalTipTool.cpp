// Created by Paweł Sobótka on 29.08.17.
// Rail-Mil Computers Sp. z o.o.

#include "SphericalTipTool.hpp"

using namespace maths;

namespace App {

    SphericalTipTool::SphericalTipTool(Qt3DCore::QEntity *parent) : Tool(parent) {
        tip_cylinder_entity = new Qt3DCore::QEntity{this};
        tip_cylinder_mesh = new Qt3DExtras::QCylinderMesh{this};
        tip_cylinder_transform = new Qt3DCore::QTransform{this};

        tip_cylinder_entity->addComponent(tip_cylinder_mesh);
        tip_cylinder_entity->addComponent(tip_cylinder_transform);
        tip_cylinder_entity->addComponent(tip_material);

        tip_sphere_entity = new Qt3DCore::QEntity{this};
        tip_sphere_mesh = new Qt3DExtras::QSphereMesh{this};
        tip_sphere_transform = new Qt3DCore::QTransform{this};

        tip_sphere_entity->addComponent(tip_sphere_mesh);
        tip_sphere_entity->addComponent(tip_sphere_transform);
        tip_sphere_entity->addComponent(tip_material);

        updateTip();
    }

    void SphericalTipTool::updateTip() {
        auto cylinder_length = cut_length - diameter / 2.0f;

        tip_cylinder_mesh->setLength(cylinder_length);
        tip_cylinder_mesh->setRadius(diameter / 2.0f);
        tip_cylinder_transform->setTranslation(QVector3D{0.0f, cylinder_length / 2.0f + diameter / 2.0f, 0.0f});

        tip_sphere_mesh->setRadius(diameter / 2.0f);
        tip_sphere_transform->setTranslation(QVector3D{0.0f, diameter / 2.0f, 0.0f});
    }

    float SphericalTipTool::intersectionDepth(const vec3 &point) const {
        const auto radius = diameter / 2.0f;

        auto axis_distance = (vec2{{point.x(), point.z()}}).length();
        auto tip_center_distance = (vec2{{axis_distance, point.y() - radius}}).length();

        bool in_cyllinder = axis_distance <= radius;
        bool in_slice = point.y() >= radius && point.y() <= cut_length;
        bool in_tip = point.y() < radius && tip_center_distance <= radius;

        if((in_cyllinder && in_slice) || in_tip) {
            return in_tip ? radius - tip_center_distance : std::min(radius - axis_distance, cut_length - point.y());
        } else {
            return -1.0f;
        }
    }

    std::string SphericalTipTool::typeName() const {
        return TypeNameString;
    }

}