// Created by Paweł Sobótka on 27.10.17.
// Rail-Mil Computers Sp. z o.o.

#include "ToolPath.hpp"

#include "PathEvaluatorToolController.hpp"

#include <Qt3DExtras/QPerVertexColorMaterial>

namespace App {

    ToolPath::ToolPath(Qt3DCore::QEntity *parent)
            : Qt3DCore::QEntity(parent),
              program{nullptr}{

        auto geometry_renderer = new Qt3DRender::QGeometryRenderer(this);
        geometry_renderer->setPrimitiveType(Qt3DRender::QGeometryRenderer::LineStrip);
        auto geometry = new Qt3DRender::QGeometry(geometry_renderer);

        vbo = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::VertexBuffer, geometry);
        ibo = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::IndexBuffer, geometry);

        positionAttr = new Qt3DRender::QAttribute(geometry);
        positionAttr->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
        positionAttr->setBuffer(vbo);
        positionAttr->setByteOffset(0);
        positionAttr->setByteStride(sizeof(vertex));
        positionAttr->setVertexSize(3);
        positionAttr->setVertexBaseType(Qt3DRender::QAttribute::VertexBaseType::Float);
        positionAttr->setCount(0);
        positionAttr->setName(Qt3DRender::QAttribute::defaultPositionAttributeName());

        colorAttr = new Qt3DRender::QAttribute(geometry);
        colorAttr->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
        colorAttr->setBuffer(vbo);
        colorAttr->setByteOffset(sizeof(vertex::pos));
        colorAttr->setByteStride(sizeof(vertex));
        colorAttr->setVertexSize(3);
        colorAttr->setVertexBaseType(Qt3DRender::QAttribute::VertexBaseType::Float);
        colorAttr->setCount(0);
        colorAttr->setName(Qt3DRender::QAttribute::defaultColorAttributeName());

        indexAttr = new Qt3DRender::QAttribute(geometry);
        indexAttr->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
        indexAttr->setBuffer(ibo);
        indexAttr->setByteOffset(0);
        indexAttr->setByteStride(sizeof(unsigned int));
        indexAttr->setVertexSize(1);
        indexAttr->setVertexBaseType(Qt3DRender::QAttribute::VertexBaseType::UnsignedInt);
        indexAttr->setCount(0);

        geometry->addAttribute(positionAttr);
        geometry->addAttribute(colorAttr);
        geometry->addAttribute(indexAttr);

        geometry_renderer->setGeometry(geometry);

        auto material = new Qt3DExtras::QPerVertexColorMaterial(this);

        this->addComponent(geometry_renderer);
        this->addComponent(material);

        generateGeometry();
    }

    void ToolPath::setProgram(ProgramModel *program) {
        if (this->program != nullptr) {
            disconnect(program, &ProgramModel::rowsInserted, this, &ToolPath::rowsInserted);
            disconnect(program, &ProgramModel::rowsRemoved, this, &ToolPath::rowsRemoved);
            disconnect(program, &ProgramModel::modelReset, this, &ToolPath::modelReset);
        }

        this->program = program;

        if (this->program != nullptr) {
            connect(program, &ProgramModel::rowsInserted, this, &ToolPath::rowsInserted);
            connect(program, &ProgramModel::rowsRemoved, this, &ToolPath::rowsRemoved);
            connect(program, &ProgramModel::modelReset, this, &ToolPath::modelReset);
        }

        generateGeometry();

        emit programChanged();
    }

    void ToolPath::rowsInserted(const QModelIndex &parent, int first, int last) {
        generateGeometry();
    }

    void ToolPath::rowsRemoved(const QModelIndex &parent, int first, int last) {
        generateGeometry();
    }

    void ToolPath::modelReset() {
        generateGeometry();
    }

    void ToolPath::generateGeometry() {

        if (this->program == nullptr) {
            vertexBuffer.resize(0);
            indexBuffer.resize(0);
        } else {

            PathEvaluatorToolController toolController;

            std::size_t totalPoints = 0;

            for (auto step = 0; step < program->stepCount(); step++) {
                const auto &stepCode = program->getStep(step);
                totalPoints += stepCode.size();
            }

            vertexBuffer.resize(totalPoints);
            indexBuffer.resize(totalPoints);

            std::size_t i = 0;

            for (auto step = 0; step < program->stepCount(); step++) {
                const auto &stepCode = program->getStep(step);
                for (const auto &cmd : stepCode) {
                    cmd->execute(&toolController);
                    vertexBuffer[i].pos = toolController.getPosition();
                    vertexBuffer[i].color = maths::vec3{{0.0f, 1.0f, 0.1f}};
                    indexBuffer[i] = static_cast<unsigned int>(i);
                    i++;
                }
            }
        }

        ibo->setData(QByteArray(reinterpret_cast<const char *>(indexBuffer.data()), static_cast<int>(indexBuffer.size() * sizeof(unsigned int))));
        vbo->setData(QByteArray(reinterpret_cast<const char *>(vertexBuffer.data()), static_cast<int>(vertexBuffer.size() * sizeof(vertex))));

        indexAttr->setCount(static_cast<unsigned int>(indexBuffer.size()));
        positionAttr->setCount(static_cast<unsigned int>(vertexBuffer.size()));

    }




}