// Created by Paweł Sobótka on 05.09.17.
// Rail-Mil Computers Sp. z o.o.

#include "ToolController.hpp"

using namespace maths;

namespace App {

    ToolController::ToolController(const maths::vec3 &initialPosition)
            : m_finished{false}, position{initialPosition}, target{initialPosition}, totalDistance{0.0f} {

    }

    void ToolController::rapidPosition(optFloat x, optFloat y, optFloat z) {
        moveTo(x, y, z);
    }

    void ToolController::linearInterpolate(optFloat x, optFloat y, optFloat z) {
        moveTo(x, y, z);
    }

    std::pair<bool, qint64> ToolController::update(qint64 msecs, bool instant)
    {
        qint64 timeLeft = msecs;

        if(m_finished)
            return {true, timeLeft};

        if(instant) {
            position = target;
            m_finished = true;
            return {m_finished, timeLeft};
        }

        vec3 displacement = target - position;

        float distanceLeft = displacement.length(); // millimeters to move

        float mmPerUpdate = 15'000 / 60.0f * msecs / 1000.0f;

        vec3 direction = displacement.normalized();

        if(distanceLeft > mmPerUpdate) {
            position += (mmPerUpdate * direction);
            totalDistance += mmPerUpdate;
            timeLeft = 0;
        } else {
            position = target;
            m_finished = true;
            totalDistance += distanceLeft;

            auto unusedDistance = mmPerUpdate - distanceLeft;
            timeLeft = static_cast<qint64>(60.0f * 1000.0f * unusedDistance / 15'000);
        }

        return {m_finished, timeLeft};
    }

    void ToolController::moveTo(optFloat x, optFloat y, optFloat z) {

        target = vec3{{
                              x.has_value() ? x.value() : position.x(),
                              y.has_value() ? y.value() : position.y(),
                              z.has_value() ? z.value() : position.z()
                      }};

        m_finished = false;
    }

    float ToolController::getTotalDistance() const {
        return totalDistance;
    }

}