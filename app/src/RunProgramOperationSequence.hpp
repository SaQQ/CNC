// Created by Paweł Sobótka on 05.09.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_RUNPROGRAMOPERATIONSEQUENCE_HPP
#define CNC_SIMULATOR_RUNPROGRAMOPERATIONSEQUENCE_HPP

#include <memory>

#include "IOperationSequence.hpp"

#include "ToolController.hpp"
#include "ProgramModel.hpp"

namespace App {

    class RunProgramOperationSequence : public IOperationSequence {
    public:
        explicit RunProgramOperationSequence(const ProgramModel* program);

        bool tick(IMillable *material, ISimulationController *sim, qint64 msecs) override;

        bool finished() override;

        float progress() override;

        void runToEnd() override;

    private:

        enum class Stage {
            NEXT_STEP_INIT,
            RUNNING_COMMAND,
            FINISHED
        } stage;

        const ProgramModel * const program;

        int currentStepIndex;
        const GCodeProgram *currentStep;
        GCodeProgram::const_iterator currentStepIterator;

        std::unique_ptr<ToolController> toolController;

        float prevStepsProgress;

        bool toEnd;
    };

}


#endif //CNC_SIMULATOR_RUNPROGRAMOPERATIONSEQUENCE_HPP
