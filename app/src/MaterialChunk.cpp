#include "MaterialChunk.hpp"

#include <set>

#include <Qt3DRender/QGeometryRenderer>

#include <Qt3DExtras/QDiffuseMapMaterial>

#include <QTextureImage>

#include <intersections.hpp>

namespace App {

    MaterialChunk::MaterialChunk(Qt3DCore::QEntity *parent) : Qt3DCore::QEntity(parent),
                                                              xCells(DefaultXCells), zCells(DefaultZCells),
                                                              xExtent(DefaultXExtent), zExtent(DefaultZExtent),
                                                              height(DefaultHeight),
                                                              heightThreshold{DefaultHeightThreshold}
    {
        auto geometry_renderer = new Qt3DRender::QGeometryRenderer(this);
        auto geometry = new Qt3DRender::QGeometry(geometry_renderer);

        vbo = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::VertexBuffer, geometry);
        ibo = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::IndexBuffer, geometry);

        vbo->setUsage(Qt3DRender::QBuffer::DynamicDraw);
        ibo->setUsage(Qt3DRender::QBuffer::DynamicDraw);

        positionAttr = new Qt3DRender::QAttribute(geometry);
        positionAttr->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
        positionAttr->setBuffer(vbo);
        positionAttr->setByteOffset(0);
        positionAttr->setByteStride(sizeof(vertex));
        positionAttr->setVertexSize(3);
        positionAttr->setVertexBaseType(Qt3DRender::QAttribute::VertexBaseType::Float);
        positionAttr->setCount(0);
        positionAttr->setName(Qt3DRender::QAttribute::defaultPositionAttributeName());

        normalAttr = new Qt3DRender::QAttribute(geometry);
        normalAttr->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
        normalAttr->setBuffer(vbo);
        normalAttr->setByteOffset(sizeof(vertex::pos));
        normalAttr->setByteStride(sizeof(vertex));
        normalAttr->setVertexSize(3);
        normalAttr->setVertexBaseType(Qt3DRender::QAttribute::VertexBaseType::Float);
        normalAttr->setCount(0);
        normalAttr->setName(Qt3DRender::QAttribute::defaultNormalAttributeName());

        texCoordAttr = new Qt3DRender::QAttribute(geometry);
        texCoordAttr->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
        texCoordAttr->setBuffer(vbo);
        texCoordAttr->setByteOffset(sizeof(vertex::pos) + sizeof(vertex::normal));
        texCoordAttr->setByteStride(sizeof(vertex));
        texCoordAttr->setVertexSize(2);
        texCoordAttr->setVertexBaseType(Qt3DRender::QAttribute::VertexBaseType::Float);
        texCoordAttr->setCount(0);
        texCoordAttr->setName(Qt3DRender::QAttribute::defaultTextureCoordinateAttributeName());

        indexAttr = new Qt3DRender::QAttribute(geometry);
        indexAttr->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
        indexAttr->setBuffer(ibo);
        indexAttr->setByteOffset(0);
        indexAttr->setByteStride(sizeof(unsigned int));
        indexAttr->setVertexSize(1);
        indexAttr->setVertexBaseType(Qt3DRender::QAttribute::VertexBaseType::UnsignedInt);
        indexAttr->setCount(0);

        geometry->addAttribute(positionAttr);
        geometry->addAttribute(normalAttr);
        geometry->addAttribute(texCoordAttr);
        geometry->addAttribute(indexAttr);

        geometry_renderer->setGeometry(geometry);

        auto material = new Qt3DExtras::QDiffuseMapMaterial(this);
        auto texture = material->diffuse();
        auto textureImage = new Qt3DRender::QTextureImage();
        textureImage->setSource(QUrl("qrc:/images/texture.png"));
        texture->addTextureImage(textureImage);

        transform = new Qt3DCore::QTransform(this);

        this->addComponent(geometry_renderer);
        this->addComponent(material);
        this->addComponent(transform);

        regenerate(xExtent, zExtent, height, xCells, zCells, 0);
    }

    void MaterialChunk::reset() {
        auto xGrains = xCells + 1;
        auto zGrains = zCells + 1;

        for (unsigned int z = 0; z < zGrains; ++z)
            for (unsigned int x = 0; x < xGrains; ++x)
                setHeight(x, z, height);

        updateAllNormals();

        vbo->updateData(0, QByteArray(reinterpret_cast<const char *>(vertexBuffer.data()), static_cast<int>(vertexBuffer.size() * sizeof(vertex))));
    }

    void MaterialChunk::regenerate(float xExtent, float zExtent, float height, unsigned int xCells, unsigned int zCells, float heightThreshold)
    {
        setXExtent(xExtent);
        setZExtent(zExtent);
        setHeight(height);
        setXCells(xCells);
        setZCells(zCells);
        setHeightThreshold(heightThreshold);

        auto xGrains = xCells + 1;
        auto zGrains = zCells + 1;

        auto surfaceVertexCount = xGrains * zGrains;
        auto sideVertexCount = 2 * (2 * xGrains + 2 * zGrains);
        auto vertexCount = surfaceVertexCount + sideVertexCount;

        vertexBuffer.resize(vertexCount);

        auto dx = xExtent / xCells;
        auto dz = zExtent / zCells;

        // Surface vertices

        surfaceVertices = vertexBuffer.data();

        for(unsigned int z = 0; z < zGrains; ++z)
        {
            for(unsigned int x = 0; x < xGrains; ++x)
            {
                auto& vertex = surfaceVertices[z * xGrains + x];
                vertex.pos = maths::vec3{{x * dx, height, z * dz}};
                vertex.normal = maths::vec3{{0.0f, 1.0f, 0.0f}};
                vertex.texCoord = maths::vec2{{x * dx / xExtent, z * dz / zExtent}};
            }
        }

        // Side vertices

        auto sideVertices = vertexBuffer.data() + surfaceVertexCount;

        negativeXG = sideVertices;
        positiveXG = negativeXG + xGrains;
        negativeZG = positiveXG + xGrains;
        positiveZG = negativeZG + zGrains;

        auto negativeXGBaseIndex = surfaceVertexCount;
        auto positiveXGBaseIndex = negativeXGBaseIndex + xGrains;
        auto negativeZGBaseIndex = positiveXGBaseIndex + xGrains;
        auto positiveZGBaseIndex = negativeZGBaseIndex + zGrains;

        negativeXD = positiveZG + zGrains;
        positiveXD = negativeXD + xGrains;
        negativeZD = positiveXD + xGrains;
        positiveZD = negativeZD + zGrains;

        auto negativeXDBaseIndex = positiveZGBaseIndex + zGrains;
        auto positiveXDBaseIndex = negativeXDBaseIndex + xGrains;
        auto negativeZDBaseIndex = positiveXDBaseIndex + xGrains;
        auto positiveZDBaseIndex = negativeZDBaseIndex + zGrains;

        for (unsigned int x = 0; x < xGrains; ++x)
        {
            negativeXG[x].pos = maths::vec3{{x * dx, height, 0.0f}};
            negativeXG[x].normal = maths::vec3{{0.0f, 0.0f, -1.0f}};
            negativeXG[x].texCoord = maths::vec2{{x * dx / xExtent, 1.0f}};

            positiveXG[x].pos = maths::vec3{{x * dx, height, zExtent}};
            positiveXG[x].normal = maths::vec3{{0.0f, 0.0f, 1.0f}};
            positiveXG[x].texCoord = maths::vec2{{x * dx / xExtent, 1.0f}};

            negativeXD[x].pos = maths::vec3{{x * dx, 0.0f, 0.0f }};
            negativeXD[x].normal = maths::vec3{{0.0f, 0.0f, -1.0f}};
            negativeXD[x].texCoord = maths::vec2{{x * dx / xExtent, 0.0f}};

            positiveXD[x].pos = maths::vec3{{x * dx, 0.0f, zExtent}};
            positiveXD[x].normal = maths::vec3{{0.0f, 0.0f, 1.0f}};
            positiveXD[x].texCoord = maths::vec2{{x * dx / xExtent, 0.0f}};

        }

        for (unsigned int z = 0; z < zGrains; ++z)
        {
            negativeZG[z].pos = maths::vec3{{0.0f, height, z * dz}};
            negativeZG[z].normal = maths::vec3{{-1.0f, 0.0f, 0.0f}};
            negativeZG[z].texCoord = maths::vec2{{1.0f, z * dz / zExtent}};

            positiveZG[z].pos = maths::vec3{{xExtent, height, z * dz}};
            positiveZG[z].normal = maths::vec3{{1.0f, 0.0f, 0.0f}};
            positiveZG[z].texCoord = maths::vec2{{1.0f, z * dz / zExtent}};

            negativeZD[z].pos = maths::vec3{{0.0f, 0.0f, z * dz}};
            negativeZD[z].normal = maths::vec3{{-1.0f, 0.0f, 0.0f}};
            negativeZD[z].texCoord = maths::vec2{{0.0f, z * dz / zExtent}};

            positiveZD[z].pos = maths::vec3{{xExtent, 0.0f, z * dz}};
            positiveZD[z].normal = maths::vec3{{1.0f, 0.0f, 0.0f}};
            positiveZD[z].texCoord = maths::vec2{{0.0f, z * dz / zExtent}};
        }

        // Indices

        auto surfaceTriangleCount = 2 * xCells * zCells;
        auto sideTriangleCount = 2 * (2 * xCells + 2 * zCells);

        indexBuffer.resize(3 * (surfaceTriangleCount + sideTriangleCount));

        // Surface indices

        auto surfaceIndices = indexBuffer.data();

        for(unsigned int z = 0; z < zCells; ++z)
        {
            for (unsigned int x = 0; x < xCells; ++x)
            {
                surfaceIndices[(z * xCells + x) * 3 + 0] = z * xGrains + x;
                surfaceIndices[(z * xCells + x) * 3 + 1] = (z + 1) * xGrains + x;
                surfaceIndices[(z * xCells + x) * 3 + 2] = z * xGrains + x + 1;

                surfaceIndices[xCells * zCells * 3 + (z * xCells + x) * 3 + 0] = z * xGrains + x + 1;
                surfaceIndices[xCells * zCells * 3 + (z * xCells + x) * 3 + 1] = (z + 1) * xGrains + x;
                surfaceIndices[xCells * zCells * 3 + (z * xCells + x) * 3 + 2] = (z + 1) * xGrains + x + 1;
            }
        }

        // Side indices

        auto sideIndices = surfaceIndices + 3 * surfaceTriangleCount;

        auto negativeXIndices = sideIndices;
        auto positiveXIndices = negativeXIndices + 3 * 2 * xCells;
        auto negativeZIndices = positiveXIndices + 3 * 2 * xCells;
        auto positiveZIndices = negativeZIndices + 3 * 2 * zCells;

        for (unsigned int x = 0; x < xCells; ++x)
        {
            negativeXIndices[x * 3 * 2 + 0] = negativeXGBaseIndex + x;
            negativeXIndices[x * 3 * 2 + 1] = negativeXGBaseIndex + x + 1;
            negativeXIndices[x * 3 * 2 + 2] = negativeXDBaseIndex + x;
            negativeXIndices[x * 3 * 2 + 3] = negativeXGBaseIndex + x + 1;
            negativeXIndices[x * 3 * 2 + 4] = negativeXDBaseIndex + x + 1;
            negativeXIndices[x * 3 * 2 + 5] = negativeXDBaseIndex + x;

            positiveXIndices[x * 3 * 2 + 0] = positiveXGBaseIndex + x;
            positiveXIndices[x * 3 * 2 + 1] = positiveXDBaseIndex + x;
            positiveXIndices[x * 3 * 2 + 2] = positiveXGBaseIndex + x + 1;
            positiveXIndices[x * 3 * 2 + 3] = positiveXGBaseIndex + x + 1;
            positiveXIndices[x * 3 * 2 + 4] = positiveXDBaseIndex + x;
            positiveXIndices[x * 3 * 2 + 5] = positiveXDBaseIndex + x + 1;
        }

        for (unsigned int z = 0; z < zCells; ++z)
        {
            negativeZIndices[z * 3 * 2 + 0] = negativeZGBaseIndex + z + 1;
            negativeZIndices[z * 3 * 2 + 1] = negativeZGBaseIndex + z;
            negativeZIndices[z * 3 * 2 + 2] = negativeZDBaseIndex + z;
            negativeZIndices[z * 3 * 2 + 3] = negativeZGBaseIndex + z + 1;
            negativeZIndices[z * 3 * 2 + 4] = negativeZDBaseIndex + z;
            negativeZIndices[z * 3 * 2 + 5] = negativeZDBaseIndex + z + 1;

            positiveZIndices[z * 3 * 2 + 0] = positiveZGBaseIndex + z;
            positiveZIndices[z * 3 * 2 + 1] = positiveZGBaseIndex + z + 1;
            positiveZIndices[z * 3 * 2 + 2] = positiveZDBaseIndex + z;
            positiveZIndices[z * 3 * 2 + 3] = positiveZGBaseIndex + z + 1;
            positiveZIndices[z * 3 * 2 + 4] = positiveZDBaseIndex + z + 1;
            positiveZIndices[z * 3 * 2 + 5] = positiveZDBaseIndex + z;
        }

        ibo->setData(QByteArray(reinterpret_cast<const char *>(indexBuffer.data()), static_cast<int>(indexBuffer.size() * sizeof(unsigned int))));
        vbo->setData(QByteArray(reinterpret_cast<const char *>(vertexBuffer.data()), static_cast<int>(vertexBuffer.size() * sizeof(vertex))));

        indexAttr->setCount(static_cast<unsigned int>(indexBuffer.size()));
        positionAttr->setCount(static_cast<unsigned int>(vertexBuffer.size()));
        texCoordAttr->setCount(static_cast<unsigned int>(vertexBuffer.size()));
        normalAttr->setCount(static_cast<unsigned int>(vertexBuffer.size()));

        transform->setTranslation(QVector3D(-xExtent / 2.0f, 0.0f, -zExtent / 2.0f));
    }

    void MaterialChunk::setHeight(std::size_t x, std::size_t z, float height)
    {
        auto xGrains = xCells + 1;
        auto zGrains = zCells + 1;

        surfaceVertices[z * xGrains + x].pos.y() = height;

        if (x == 0) {
            negativeZG[z].pos.y() = height;
            negativeZG[z].texCoord.x() = height / this->height;
        } else if (x == xGrains - 1) {
            positiveZG[z].pos.y() = height;
            positiveZG[z].texCoord.x() = height / this->height;
        }

        if(z == 0) {
            negativeXG[x].pos.y() = height;
            negativeXG[x].texCoord.y() = height / this->height;
        }  else if (z == zGrains - 1) {
            positiveXG[x].pos.y() = height;
            positiveXG[x].texCoord.y() = height / this->height;
        }
    }

    void MaterialChunk::updateNormal(std::size_t x, std::size_t z)
    {
        auto xGrains = xCells + 1;
        auto zGrains = zCells + 1;

        maths::vec3 normal{{0.0f, 0.0f, 0.0f}};

        auto mx = x > 0;
        auto px = x < xGrains - 1;
        auto mz = z > 0;
        auto pz = z < zGrains - 1;

        auto pos = surfaceVertices[(z) * xGrains + (x)].pos;

        if (mz && mx) {
            normal += maths::cross((surfaceVertices[(z - 1) * xGrains + (x)].pos - pos).normalized(),
                                   (surfaceVertices[(z) * xGrains + (x - 1)].pos - pos).normalized());
        }

        if (mx & pz) {
            normal += maths::cross((surfaceVertices[(z) * xGrains + (x - 1)].pos - pos).normalized(),
                                   (surfaceVertices[(z + 1) * xGrains + (x)].pos - pos).normalized());
        }

        if (pz && px) {
            normal += maths::cross((surfaceVertices[(z + 1) * xGrains + (x)].pos - pos).normalized(),
                                   (surfaceVertices[(z) * xGrains + (x + 1)].pos - pos).normalized());
        }

        if (px && mz) {
            normal += maths::cross((surfaceVertices[(z) * xGrains + (x + 1)].pos - pos).normalized(),
                                   (surfaceVertices[(z - 1) * xGrains + (x)].pos - pos).normalized());
        }

        normal.normalize();
        surfaceVertices[z * xGrains + x].normal = normal;
    }

    void MaterialChunk::millSpherical(const maths::vec3 &startPos, const maths::vec3 &endPos, float radius,
                                      bool updateBuffers)
    {

        if (startPos.y() > height && endPos.y() > height)
            return;

        if (startPos.x() + radius < 0.0f && endPos.x() + radius < 0.0f)
            return;

        if (startPos.z() + radius < 0.0f && endPos.z() + radius < 0.0f)
            return;

        if (startPos.x() - radius > xExtent && endPos.x() - radius > xExtent)
            return;

        if (startPos.z() - radius > zExtent && endPos.z() - radius > zExtent)
            return;

        maths::capsule capsule{startPos + maths::vec3{{0.0f, radius, 0.0f}}, endPos + maths::vec3{{0.0f, radius, 0.0f}}, radius};
        maths::intersection_cache<maths::capsule> cache{capsule};

        auto heightFunction = [&startPos, &endPos, &capsule, &cache](float x, float z) {
            bool intersection;
            float t1, t2;
            std::tie(intersection, t1, t2) = maths::ray_capsule_intersection({maths::vec3{{x, 0.0f, z}}, maths::vec3{{0.0f, 1.0f, 0.0f}}}, capsule, cache);

            if (intersection)
                return t1;
            else return 1000.0f;
        };

        mill(startPos, endPos, radius, heightFunction, updateBuffers);
    }

    void MaterialChunk::millFlat(const maths::vec3 &startPos, const maths::vec3 &endPos, float radius,
                                 bool updateBuffers)
    {
        if (startPos.y() > height && endPos.y() > height)
            return;

        if (startPos.x() + radius < 0.0f && endPos.x() + radius < 0.0f)
            return;

        if (startPos.z() + radius < 0.0f && endPos.z() + radius < 0.0f)
            return;

        if (startPos.x() - radius > xExtent && endPos.x() - radius > xExtent)
            return;

        if (startPos.z() - radius > zExtent && endPos.z() - radius > zExtent)
            return;

        maths::flat_capsule capsule{startPos, endPos, radius};
        maths::intersection_cache<maths::flat_capsule> cache{capsule};

        auto heightFunction = [&startPos, &endPos, &capsule, &cache](float x, float z) {
            bool intersection;
            float t;
            std::tie(intersection, t) = maths::ray_flat_capsule_intersection(maths::vec3{{x, 0.0f, z}}, capsule, cache);

            if (intersection)
                return t;
            else return 1000.0f;
        };

        auto grainsMilled = mill(startPos, endPos, radius, heightFunction, updateBuffers);

        if (grainsMilled > 0 && startPos.y() > endPos.y())
            throw MillingError{"Milling vertically with flat tool"};
    }

    void MaterialChunk::updateAllNormals()
    {
        for (unsigned int z = 0; z <= zCells; ++z)
            for (unsigned int x = 0; x <= xCells; ++x)
                updateNormal(x, z);
    }

    void MaterialChunk::fullBufferUpdate()
    {
        vbo->updateData(0, QByteArray(reinterpret_cast<const char *>(vertexBuffer.data()), static_cast<int>(vertexBuffer.size() * sizeof(vertex))));
    }

    bool MaterialChunk::millGrain(std::size_t x, std::size_t z,
                                  MaterialChunk::machineStripHeightFunction heightFunction)
    {
        auto xGrains = xCells + 1;
//        auto zGrains = zCells + 1;

        auto dx = xExtent / xCells;
        auto dz = zExtent / zCells;

        auto h = heightFunction(x * dx, z * dz);

        if (h < heightThreshold)
            throw MillingError{"Material milled below allowed threshold"};

        if (surfaceVertices[z * xGrains + x].pos.y() > h)
        {
            setHeight(x, z, h);
            return true;
        }

        return false;
    }

    void MaterialChunk::processGrains(const maths::vec3 &startPos, const maths::vec3 &endPos, float radius, processGrainFunction process, gainsLineProcessedCallback lineProcessed)
    {
        auto start = maths::vec2{{startPos.x(), startPos.z()}};
        auto end = maths::vec2{{endPos.x(), endPos.z()}};

        auto dx = xExtent / xCells;
        auto dz = xExtent / zCells;

        float extendedRadius = radius + 4.0f * std::max(dx, dz);

        maths::vec2 path = end - start;

        maths::vec2 dir;
        maths::vec2 perpendicularDir;

        if (path.length_squared() < std::min(dx, dz) * std::min(dx, dz))
            dir = maths::vec2{{1.0f, 0.0f}};
        else
            dir = path.normalized();

        perpendicularDir = maths::vec2{{dir.y(), -dir.x()}};

        maths::vec2 s1 = start - extendedRadius * dir + extendedRadius * perpendicularDir;
        maths::vec2 s2 = start - extendedRadius * dir - extendedRadius * perpendicularDir;

        maths::vec2 e1 = end + extendedRadius * dir + extendedRadius * perpendicularDir;
        maths::vec2 e2 = end + extendedRadius * dir - extendedRadius * perpendicularDir;

        processRectangle(s1, e1, e2, s2, [this, process](int x, int z) {
            if (x >= 0 && z >= 0 && x <= static_cast<int>(xCells) && z <= static_cast<int>(zCells))
                process(static_cast<std::size_t>(x), static_cast<std::size_t>(z));
        }, [this, lineProcessed](int z, int sx, int ex) {
            if (z < 0 || z > static_cast<int>(zCells))
                return;
            if (ex < 0)
                return;
            if (sx > static_cast<int>(xCells))
                return;
            if (sx >= ex)
                return;

            sx = std::max(0, sx);
            ex = std::min(static_cast<int>(xCells), ex);

            lineProcessed(static_cast<std::size_t>(z), static_cast<std::size_t>(sx), static_cast<std::size_t>(ex));
        });

    }

    void MaterialChunk::processRectangle(const maths::vec2 &v1, const maths::vec2 &v2, const maths::vec2 &v3, const maths::vec2 &v4, fillFunction fill, lineProcessedCallback lineProcessed) {

        struct edge {
            int minY;
            int maxY;
            float x;
            float dxdy;
        };

        struct lessX {
            constexpr bool operator()(const edge& lhs, const edge& rhs) {
                return lhs.x < rhs.x;
            }
        };

        struct lessMinY {
            constexpr bool operator()(const edge& lhs, const edge& rhs) {
                return lhs.minY < rhs.minY;
            }
        };

        using ivec2 = maths::vec<int, 2>;

        auto dx = xExtent / xCells;
        auto dz = zExtent / zCells;

        auto coordGrain = [](float coord, float d) {
            return static_cast<int>(std::floor((coord + d / 2) / d));
        };

        auto pointGrain = [coordGrain, dx, dz](const maths::vec2 &v) {
            return ivec2{{coordGrain(v.x(), dx), coordGrain(v.y(), dz)}};
        };

        auto createEdge = [](ivec2 start, ivec2 end) {
            edge e;
            if (start.y() > end.y()) {
                std::swap(start.x(), end.x());
                std::swap(start.y(), end.y());
            }
            e.minY = start.y();
            e.maxY = end.y();
            e.x = static_cast<float>(start.x());
            e.dxdy = static_cast<float>(end.x() - start.x()) / static_cast<float>(end.y() - start.y());
            return e;
        };

        // Choose minY and maxY vertex

        auto i1 = pointGrain(v1);
        auto i2 = pointGrain(v2);
        auto i3 = pointGrain(v3);
        auto i4 = pointGrain(v4);

        std::multiset<edge, lessMinY> edges;

        if (i1.y() != i2.y())
            edges.insert(createEdge(i1, i2));
        if (i2.y() != i3.y())
            edges.insert(createEdge(i2, i3));
        if(i3.y() != i4.y())
            edges.insert(createEdge(i3, i4));
        if(i4.y() != i1.y())
            edges.insert(createEdge(i4, i1));

        std::multiset<edge, lessX> aet;
        std::multiset<edge, lessX> nextAet;

        if (edges.size() == 0)
            return;

        auto minY = edges.begin()->minY;
        auto maxY = edges.rbegin()->maxY;

        for (auto y = minY; y <= maxY; ++y) {
            for (auto it = edges.begin(); it != edges.end() && it->minY <= y;) {
                if (it->minY == y) {
                    aet.insert(*it);
                    it = edges.erase(it);
                } else {
                    ++it;
                }
            }
            for (auto it = aet.begin(); it != aet.end();) {
                if (it->maxY == y) {
                    it = aet.erase(it);
                } else {
                    ++it;
                }
            }
            for (auto it = aet.begin(); it != aet.end();) {
                auto e1 = *it;
                it = aet.erase(it);
                auto e2 = *it;
                it = aet.erase(it);

                for(auto x = e1.x; x <= e2.x; ++x)
                    fill(x, y);

                lineProcessed(y, e1.x, e2.x);

                e1.x += e1.dxdy;
                e2.x += e2.dxdy;

                nextAet.insert(e1);
                nextAet.insert(e2);
            }

            aet = std::move(nextAet);
            nextAet.clear();
        }

    }

    std::size_t MaterialChunk::mill(const maths::vec3 &startPos, const maths::vec3 &endPos, float radius,
                               MaterialChunk::machineStripHeightFunction heightFunction, bool updateBuffers)
    {

        // grains are processed by const-Z lines

        std::vector<std::tuple<std::size_t, std::size_t, std::size_t>> lines;
        std::size_t totalGrainsMilled = 0;

        processGrains(startPos, endPos, radius, [this, heightFunction, &totalGrainsMilled](std::size_t x, std::size_t z) {
            totalGrainsMilled += millGrain(x, z, heightFunction) ? 1 : 0;
        }, [&lines](std::size_t z, std::size_t sx, std::size_t ex){
            lines.push_back({z, sx, ex});
        });

        auto xGrains = xCells + 1;
        auto zGrains = zCells + 1;

        if (!updateBuffers)
            return totalGrainsMilled;

        for(const auto& line : lines) {
            std::size_t z, sx, ex;
            std::tie(z, sx, ex) = line;

            for(auto x = sx; x <= ex; ++x)
                updateNormal(x, z);

            auto first_index = z * xGrains + sx;
            auto last_index = z * xGrains + ex;
            auto vertex_count = last_index - first_index + 1;

            vbo->updateData(static_cast<int>(first_index * sizeof(vertex)),
                            QByteArray(reinterpret_cast<const char *>(surfaceVertices + first_index), static_cast<int>(vertex_count * sizeof(vertex))));
        }

        auto surfaceVertexCount = xGrains * zGrains;
        auto sideVertexCount = 2 * xGrains + 2 * zGrains;

        vbo->updateData(surfaceVertexCount * sizeof(vertex),
                        QByteArray(reinterpret_cast<const char *>(negativeXG), static_cast<int>((sideVertexCount) * sizeof(vertex))));

        return totalGrainsMilled;
    }

    maths::vec3 MaterialChunk::toLocalSpace(const maths::vec3 &v)
    {
        return v + maths::vec3{{xExtent / 2.0f, 0.0f, zExtent / 2.0f}};
    }

}

