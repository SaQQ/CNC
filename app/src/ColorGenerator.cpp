// Created by Paweł Sobótka on 27.10.17.
// Rail-Mil Computers Sp. z o.o.

#include "ColorGenerator.hpp"

namespace App {

    ColorGenerator::ColorGenerator(QObject *parent)
            : QObject(parent) {
        colors[0] = QColor::fromRgb(255, 0, 0);
        colors[1] = QColor::fromRgb(0, 255, 0);
        colors[2] = QColor::fromRgb(0, 0, 255);
        colors[3] = QColor::fromRgb(255, 255, 0);
        colors[4] = QColor::fromRgb(255, 0, 255);
        colors[5] = QColor::fromRgb(0, 255, 255);
    }

    QColor ColorGenerator::getColor(int index) {
        return colors[index % colors.size()];
    }


}