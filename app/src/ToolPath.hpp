// Created by Paweł Sobótka on 27.10.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_TOOLPATH_HPP
#define CNC_SIMULATOR_TOOLPATH_HPP

#include <Qt3DCore/QEntity>
#include <Qt3DCore/QTransform>

#include <Qt3DRender/QBuffer>
#include <Qt3DRender/QAttribute>

#include "ProgramModel.hpp"

namespace App {

    class ToolPath : public Qt3DCore::QEntity {
        Q_OBJECT

        Q_PROPERTY(ProgramModel* program READ getProgram WRITE setProgram NOTIFY programChanged)

        struct vertex {
            maths::vec3 pos;
            maths::vec3 color;
        };
    public:
        explicit ToolPath(Qt3DCore::QEntity *parent = nullptr);

        ProgramModel* getProgram() {
            return program;
        }

        void setProgram(ProgramModel* program);

    public slots:
        void rowsInserted(const QModelIndex& parent, int first, int last);
        void rowsRemoved(const QModelIndex& parent, int first, int last);
        void modelReset();

    signals:
        void programChanged();

    private:
        ProgramModel* program;

        std::vector<vertex> vertexBuffer;
        std::vector<unsigned int> indexBuffer;

        Qt3DRender::QBuffer* vbo;
        Qt3DRender::QBuffer* ibo;

        Qt3DRender::QAttribute* positionAttr;
        Qt3DRender::QAttribute* normalAttr;
        Qt3DRender::QAttribute* colorAttr;
        Qt3DRender::QAttribute* indexAttr;

        void generateGeometry();
    };



}

#endif //CNC_SIMULATOR_TOOLPATH_HPP
