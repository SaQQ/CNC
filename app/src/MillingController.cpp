// Created by Paweł Sobótka on 27.10.17.
// Rail-Mil Computers Sp. z o.o.

#include "MillingController.hpp"

#include "RunToEndWorker.hpp"

namespace App {

    MillingController::MillingController(QObject *parent)
            : QObject(parent),
              state{"IDLE"}, progress{0.0f}, speedMultiplier{1.0f},
              material{nullptr}, toolset{nullptr}, program{nullptr}
    {
        timer = new QTimer{this};
        timer->setInterval(updateMsecInterval);
        connect(timer, &QTimer::timeout, this, &MillingController::tick);
        timer->start();
        elapsedTimer = new QElapsedTimer{};
        elapsedTimer->start();

        auto runToEndWorker = new RunToEndWorker;
        runToEndWorker->moveToThread(&workerThread);
        connect(&workerThread, &QThread::finished, runToEndWorker, &QObject::deleteLater);
        connect(this, &MillingController::startRunToEndWork, runToEndWorker, &RunToEndWorker::run);
        connect(runToEndWorker, &RunToEndWorker::progressChanged, this, &MillingController::runToEndWorkerProgressChanged);
        connect(runToEndWorker, &RunToEndWorker::error, this, &MillingController::runToEndWorkerError);
        connect(runToEndWorker, &RunToEndWorker::done, this, &MillingController::runToEndWorkerFinished);
        workerThread.start();
    }

    MillingController::~MillingController() {
        workerThread.quit();
        workerThread.wait();
    }

    bool MillingController::start() {
        if (state != "IDLE")
            return false;
        operation = std::make_unique<RunProgramOperationSequence>(program);
        toolset->setLocked(true);
        program->setLocked(true);
        setState("RUNNING");
        return true;
    }

    bool MillingController::reset() {
        if (state == "RUNNING_TO_END")
            return false;
        operation = nullptr;
        setProgress(0.0f);
        setCurrentTool(nullptr);
        toolset->setLocked(false);
        program->setLocked(false);
        material->reset();
        setState("IDLE");
        return true;
    }

    bool MillingController::runToEnd() {
        if (state == "RUNNING_TO_END" || state == "FINISHED")
            return false;
        toolset->setLocked(true);
        program->setLocked(true);
        if (operation != nullptr)
            emit startRunToEndWork(operation.release(), material);
        else
            emit startRunToEndWork(new RunProgramOperationSequence(program), material);
        setState("RUNNING_TO_END");
        return true;
    }

    void MillingController::setCurrentTool(Tool *tool) {
        if(currentTool != tool) {
            if(currentTool != nullptr)
                currentTool->setEnabled(false);
            currentTool = tool;
            if(currentTool != nullptr)
                currentTool->setEnabled(true);
            emit currentToolChanged();
        }
    }

    void MillingController::tick() {
        auto elapsed = elapsedTimer->restart();

        if (state != "RUNNING")
            return;

        bool finished = false;

        try {
            finished = operation->tick(material, this, static_cast<qint64>(elapsed * speedMultiplier));
            setProgress(operation->progress());
        } catch (const MaterialChunk::MillingError& e) {
            emit millingError(e.what());
            finished = true;
        }

        if (finished) {
            operation = nullptr;
            toolset->setLocked(false);
            program->setLocked(false);
            setState("FINISHED");
        }
    }

    void MillingController::programValidChanged()
    {
        if (program->getValid()) {
            setState("IDLE");
        } else {
            setState("PROGRAM_INVALID");
        }
    }

    void MillingController::runToEndWorkerProgressChanged(float progress) {
        this->setProgress(progress);
    }

    void MillingController::runToEndWorkerError(const QString &message)
    {
        runToEndWorkerFinished();
        emit millingError(message);
    }

    void MillingController::runToEndWorkerFinished() {
        material->updateAllNormals();
        material->fullBufferUpdate();
        toolset->setLocked(false);
        program->setLocked(false);
        setCurrentTool(nullptr);
        setState("FINISHED");
    }

}