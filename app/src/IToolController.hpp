// Created by Paweł Sobótka on 05.09.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_ITOOLCONTROLLER_HPP
#define CNC_SIMULATOR_ITOOLCONTROLLER_HPP

#include <optional>

namespace App {

    constexpr static int updateMsecInterval = 8;

    class IToolController {
    public:
        using optFloat = std::optional<float>;

        /*G00*/
        virtual void rapidPosition(optFloat x, optFloat y, optFloat z) = 0;

        /*G01*/
        virtual void linearInterpolate(optFloat x, optFloat y, optFloat z) = 0;

        virtual float getTotalDistance() const = 0;
    };

}


#endif //CNC_SIMULATOR_ITOOLCONTROLLER_HPP
