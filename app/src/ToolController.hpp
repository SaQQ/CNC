// Created by Paweł Sobótka on 05.09.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_TOOLCONTROLLER_HPP
#define CNC_SIMULATOR_TOOLCONTROLLER_HPP

#include "IToolController.hpp"

#include <utility>

#include "Tool.hpp"

namespace App {

    class ToolController : public IToolController {
    public:
        explicit ToolController(const maths::vec3 &initialPosition);

        void rapidPosition(optFloat x, optFloat y, optFloat z) override;

        void linearInterpolate(optFloat x, optFloat y, optFloat z) override;

        bool finished() {
            return m_finished;
        }

        std::pair<bool, qint64> update(qint64 msecs, bool instant = false);

        float getTotalDistance() const override;

        maths::vec3 getPosition() {
            return position;
        }

    private:

        bool m_finished;
        maths::vec3 position;
        maths::vec3 target;

        void moveTo(optFloat x, optFloat y, optFloat z);

        float totalDistance;
    };

}

#endif //CNC_SIMULATOR_TOOLCONTROLLER_HPP
