// Created by Paweł Sobótka on 04.09.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_PATHEVALUATORTOOLCONTROLLER_HPP
#define CNC_SIMULATOR_PATHEVALUATORTOOLCONTROLLER_HPP

#include <optional>

#include <vector.hpp>

#include "IToolController.hpp"

namespace App {

    class PathEvaluatorToolController final : public IToolController {
    public:
        PathEvaluatorToolController();

        void rapidPosition(optFloat x, optFloat y, optFloat z) override;

        void linearInterpolate(optFloat x, optFloat y, optFloat z) override;

        float getTotalDistance() const override {
            return totalDistance;
        }

        maths::vec3 getPosition() {
            return lastPosition;
        }

    private:
        maths::vec3 lastPosition;

        float totalDistance;

        void moveTo(optFloat x, optFloat y, optFloat z);
    };

}

#endif //CNC_SIMULATOR_PATHEVALUATORTOOLCONTROLLER_HPP
