// Created by Paweł Sobótka on 30.08.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_TOOLSETMODEL_HPP
#define CNC_SIMULATOR_TOOLSETMODEL_HPP

#include <vector>

#include <QAbstractListModel>

#include <Qt3DCore/QEntity>

#include "MachineConstants.hpp"
#include "FlatTipTool.hpp"
#include "SphericalTipTool.hpp"

namespace App {

    class ToolsetModel : public QAbstractListModel {
        Q_OBJECT

        Q_PROPERTY(bool locked READ getLocked WRITE setLocked NOTIFY lockedChanged)
        Q_PROPERTY(Qt3DCore::QEntity* sceneRoot READ getSceneRoot WRITE setSceneRoot NOTIFY sceneRootChanged)
    public:
        enum ToolsetModelRoles {
            TypeRole = Qt::UserRole + 1,
            DiameterRole,
            CutLengthRole,
            LengthRole
        };

        explicit ToolsetModel(QObject *parent = nullptr);

        bool getLocked() const {
            return locked;
        }

        void setLocked(bool value) {
            if(locked != value) {
                locked = value;
                emit lockedChanged();
            }
        }

        Qt3DCore::QEntity *getSceneRoot() const {
            return sceneRoot;
        }

        void setSceneRoot(Qt3DCore::QEntity *sceneRoot);

        QHash<int, QByteArray> roleNames() const override;

        int rowCount(const QModelIndex &parent = QModelIndex{}) const override;

        Qt::ItemFlags flags(const QModelIndex &index) const override;

        QVariant data(const QModelIndex &index, int role) const override;

        Q_INVOKABLE bool setData(const QModelIndex &index, const QVariant &value, int role) override;

        Q_INVOKABLE bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex{}) override;

        Q_INVOKABLE bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex{}) override;

        Q_INVOKABLE bool addTool(const QString &type, unsigned int diameter);

        Q_INVOKABLE int findIndex(Tool* tool) const;

        Q_INVOKABLE Tool* findTool(const QString &type, unsigned int diameter);

        Q_INVOKABLE QVariant getTool(int index);

    signals:
        void lockedChanged();
        void sceneRootChanged();

        void toolRemoved(Tool* tool);
        void toolAdded(Tool* tool);
        void toolReplaced(Tool *oldTool, Tool *newTool);

    private:
        std::vector<Tool*> tools;

        Qt3DCore::QEntity *sceneRoot;

        bool locked;

    };

}

#endif //CNC_SIMULATOR_TOOLSETMODEL_HPP
