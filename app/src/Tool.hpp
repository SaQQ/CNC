// Created by Paweł Sobótka on 25.08.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_TOOL_HPP
#define CNC_SIMULATOR_TOOL_HPP

#include <QObject>

#include <QVector3D>

#include <Qt3DCore/QEntity>

#include <Qt3DCore/QTransform>

#include <Qt3DExtras/QCylinderMesh>
#include <Qt3DExtras/QGoochMaterial>

#include <vector.hpp>

namespace App {

    class Tool : public Qt3DCore::QEntity {
        Q_OBJECT

        Q_PROPERTY(QString type READ getType CONSTANT)
        Q_PROPERTY(unsigned int diameter READ getDiameter WRITE setDiameter NOTIFY diameterChanged)
        Q_PROPERTY(unsigned int cutLength READ getCutLength WRITE setCutLength NOTIFY cutLengthChanged)
        Q_PROPERTY(unsigned int length READ getLength WRITE setLength NOTIFY lengthChanged)
        Q_PROPERTY(QVector3D position READ getPosition WRITE setPosition NOTIFY positionChanged)
    public:
        constexpr static unsigned int DefaultDiameter = 10;
        constexpr static unsigned int DefaultCutLength = 40;
        constexpr static unsigned int DefaultLength = 100;

        constexpr static unsigned int MaxDiameter = 30;

        explicit Tool(Qt3DCore::QEntity *parent = nullptr);

        unsigned int getDiameter() {
            return diameter;
        }

        void setDiameter(unsigned int value) {
            auto clampedValue = value > MaxDiameter ? MaxDiameter : value;
            if(diameter != clampedValue) {
                diameter = clampedValue;
                updateShaft();
                updateTip();
                emit diameterChanged(clampedValue);
            }
        }

        unsigned int getCutLength() {
            return cut_length;
        }

        void setCutLength(unsigned int value) {
            if(cut_length != value) {
                cut_length = value;
                updateShaft();
                updateTip();
                emit cutLengthChanged(value);
            }
        }

        unsigned int getLength() {
            return length;
        }

        void setLength(unsigned int value) {
            if(length != value) {
                length = value;
                updateShaft();
                updateTip();
                emit lengthChanged(value);
            }
        }

        QVector3D getPosition() {
            return transform->translation();
        }

        void setPosition(const QVector3D &value) {
            transform->setTranslation(value);
            emit positionChanged(value);
        }

        QString getType() const {
            return QString::fromStdString(typeName());
        }

        virtual std::string typeName() const = 0;

        virtual float intersectionDepth(const maths::vec3 &point) const = 0;

    signals:
        void diameterChanged(unsigned int newValue);
        void cutLengthChanged(unsigned int newValue);
        void lengthChanged(unsigned int newValue);
        void positionChanged(const QVector3D &newValue);
    protected:
        unsigned int diameter, cut_length, length;

        Qt3DCore::QTransform* transform;

        Qt3DExtras::QGoochMaterial* tip_material;

        Qt3DCore::QEntity* shaft_entity;
        Qt3DExtras::QCylinderMesh* shaft_mesh;
        Qt3DCore::QTransform* shaft_transform;
        Qt3DExtras::QGoochMaterial* shaft_material;

        virtual void updateTip();

    private:
        void updateShaft();
    };

}



#endif //CNC_SIMULATOR_TOOL_HPP
