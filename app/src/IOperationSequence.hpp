// Created by Paweł Sobótka on 05.09.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_IOPERATIONSEQUENCE_HPP
#define CNC_SIMULATOR_IOPERATIONSEQUENCE_HPP

#include "ISimulationController.hpp"
#include "IMillable.hpp"

namespace App {

    class IOperationSequence {
    public:
        virtual ~IOperationSequence() = default;
        virtual bool tick(IMillable *material, ISimulationController *sim, qint64 msecs) = 0;
        virtual bool finished() = 0;
        virtual float progress() = 0;
        virtual void runToEnd() = 0;
    };

}

#endif //CNC_SIMULATOR_IOPERATIONSEQUENCE_HPP
