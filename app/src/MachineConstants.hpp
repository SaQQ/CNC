// Created by Paweł Sobótka on 31.08.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_MACHINECONSTANTS_HPP
#define CNC_SIMULATOR_MACHINECONSTANTS_HPP

namespace App {

    class MachineConstants final {
    public:
        constexpr static float WorkspaceMinY = 0.0f;
        constexpr static float WorkspaceMaxY = 100.0f;
        constexpr static float WorkspaceMinX = -100.0f;
        constexpr static float WorkspaceMaxX = 100.0f;
        constexpr static float WorkspaceMinZ = -100.0f;
        constexpr static float WorkspaceMaxZ = 100.0f;

        static_assert(WorkspaceMaxX > WorkspaceMinX);
        static_assert(WorkspaceMaxY > WorkspaceMinY);
        static_assert(WorkspaceMaxZ > WorkspaceMinZ);
    };

}

#endif //CNC_SIMULATOR_MACHINECONSTANTS_HPP
