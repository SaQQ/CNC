// Created by Paweł Sobótka on 27.10.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_RUNTOENDWORKER_HPP
#define CNC_SIMULATOR_RUNTOENDWORKER_HPP

#include <QObject>

#include "ProgramModel.hpp"
#include "MaterialChunk.hpp"

#include "RunProgramOperationSequence.hpp"

namespace App {

    class RunToEndWorker : public QObject {
        Q_OBJECT
    public slots:
        void run(RunProgramOperationSequence *operation, MaterialChunk *material);
    signals:
        void done();
        void error(const QString& message);
        void progressChanged(float progress);
    };

}

#endif //CNC_SIMULATOR_RUNTOENDWORKER_HPP
