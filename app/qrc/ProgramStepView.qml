import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

import "controls"

Item {
    id: root

    width: 50
    height: 300

    property var programModel
    property var toolsetModel

    signal add()

    RoundedPlate {

        anchors.fill: parent

        RoundedButton {
            id: addButton

            height: 20

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            text: "Add"

            color: "#8BC34A"

            enabled: !root.programModel.locked

            onClicked: root.add()
        }

        ListView {
            id: programStepsListView

            currentIndex: -1

            anchors {
                top: addButton.bottom
                topMargin: 5
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }

            delegate: stepDelegate

            model: root.programModel

            spacing: 3

            displaced: Transition {
                NumberAnimation { properties: "x,y"; duration: 200 }
            }

            move: Transition {
                NumberAnimation { properties: "x,y"; duration: 200 }
            }

        }

    }

    Popup {
        id: stepPopup
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        modal: true
        focus: true

        width: stepCardr.width
        height: stepCardr.height

        clip: true

        padding: 0

        x: parent.width + 10
        y: 0

        property var modelIndex: -1
        property var model: null

        StepCard {
            id: stepCardr

            enabled: !root.programModel.locked

            stepNumber: stepPopup.modelIndex + 1
            fileName: stepPopup.model ? stepPopup.model.fileName : "<unknown>"
            pathLength: stepPopup.model ? stepPopup.model.pathLength : -1.0
            tool: stepPopup.model ? stepPopup.model.tool : null

            toolsetModel: root.toolsetModel

            onRemove: {
                stepPopup.close()
                stepPopup.model = null
                root.programModel.removeRows(stepPopup.modelIndex, 1)
                stepPopup.modelIndex = -1
            }

            onChangeTool: {
                // TODO: Assigning null to property raises TypeError
                stepPopup.model.tool = tool
            }

        }

    }

    Component {
        id: stepDelegate

        Rectangle {
            id: root

            anchors {
                left: parent.left
                right: parent.right
                margins: clickArea.containsMouse ? -1 : 0
            }

            height: width

            border {
                width: 1
                color: "#757575"
            }

            radius: 3

            Image {
                anchors.fill: parent
                source: model.tool === null ? "qrc:///images/question_mark.svg" : (model.tool.type === "flat" ? "qrc:///images/flat_tip_tool.svg" : "qrc:///images/spherical_tip_tool.svg")
            }

            Text {
                anchors {
                    bottom: parent.bottom
                    right: parent.right
                    margins: 3
                }
                font.pointSize: 8
                text: model.tool === null ? "" : ("Ø" + model.tool.diameter)
            }

            MouseArea {
                id: clickArea

                anchors.fill: parent

                hoverEnabled: true

                onClicked: {
                    stepPopup.model = model
                    stepPopup.modelIndex = index

                    stepPopup.y = root.y
                    stepPopup.open()
                }
            }

        }

    }

}
