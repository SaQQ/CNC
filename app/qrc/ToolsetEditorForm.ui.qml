import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

import "controls"

Item {
    id: root
    width: 600
    height: 400
    property alias addToolButton: addToolButton
    property alias toolsListView: toolsListView

    ListView {
        id: toolsListView
        spacing: 6
        anchors.fill: parent
        currentIndex: -1

        anchors {
            margins: 0
        }

        model: ListModel {
            ListElement {
                type: "spherical"
                diameter: 5
                cutLength: 20
                length: 80
            }

            ListElement {
                type: "spherical"
                diameter: 1
                cutLength: 5
                length: 80
            }

            ListElement {
                type: "flat"
                diameter: 16
                cutLength: 30
                length: 100
            }

            ListElement {
                type: "flat"
                diameter: 5
                cutLength: 20
                length: 80
            }
        }

        clip: true
    }

    Row {
        id: row
        spacing: 4
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0

        RoundedButton {
            id: addToolButton

            width: 80

            text: qsTr("New Tool")

            color: "#8BC34A"
        }
    }
}
