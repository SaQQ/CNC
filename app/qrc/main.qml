import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

import QtQuick.Scene3D 2.0

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

import CNCSimulator.Tools 1.0
import CNCSimulator.Simulation 1.0
import CNCSimulator 1.0

ApplicationWindow {
    id: window
    objectName: "window"
    visible: true
    title: "CNC Simulator"

    width: 1200
    height: 800

    FileDialog {
        id: openFileDialog
        title: "Please choose a file"
        folder: shortcuts.desktop
        selectExisting: true
        selectFolder: false
        selectMultiple: false
        onAccepted: {
            program.loadStep(openFileDialog.fileUrl)
        }
    }

    MessageDialog {
        id: gCodeErrorDialog
        title: "G-Code parse error"
        text: "An error was encountered while parsing G-Code"
        icon: StandardIcon.Warning
        standardButtons: StandardButton.Ok
    }

    MessageDialog {
        id: simulationErrorDialog
        title: "Simulation error"
        text: "Invalid machine move was detected"
        icon: StandardIcon.Warning
        standardButtons: StandardButton.Ok
    }

    MessageDialog {
        id: toolRequestedDialog
        title: "Tool not found"
        text: "Tool for loaded file not found in toolset. Do you want to add it to the toolset now?"
        informativeText: "Requested tool: " + type + " Ø" + diameter
        icon: StandardIcon.Question
        standardButtons: StandardButton.Yes | StandardButton.No

        onYes: {
            toolset.addTool(type, diameter)
        }

        property string type: ""
        property int diameter: 0
    }

    header: ToolBar {
        Row {
            anchors.fill: parent
            spacing: 4

            ToolButton {
                text: "Program"
                onClicked: programMenu.open()
                Menu {
                    id: programMenu
                    y: parent.height

                    MenuItem {
                        text: "Load from file"
                        onTriggered: openFileDialog.open()
                    }
                }
            }

            ToolButton {
                text: "Toolsets"
                onClicked: toolsetsMenu.open()
                Menu {
                    id: toolsetsMenu
                    y: parent.height

                    MenuItem {
                        text: "Browse"
                        onTriggered: toolsetsPopup.open()
                    }
                }
            }
            ToolButton {
                text: "Material"
                onClicked: materialMenu.open()
                Menu {
                    id: materialMenu
                    y: parent.height

                    MenuItem {
                        text: "Configure"
                        onTriggered: materialPopup.open()
                    }
                }
            }
        }
    }

    footer: ToolBar {
        height: 24
        Row {
            anchors.fill: parent
            anchors.margins: 4

            Label {
                text: controller.state
            }
        }
    }

    Popup {
        id: toolsetsPopup
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        modal: true
        focus: true

        clip: true
        padding: 10

        width: parent.width - 100
        height: parent.height - 100

        x: 50
        y: 50

        ToolsetEditor {
            anchors.fill: parent
            model: toolset
        }

    }

    Popup {
        id: materialPopup
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        modal: true
        focus: true

        clip: true
        padding: 10

        width: parent.width - 400
        height: 350

        x: 200
        y: 100

        MaterialConfigurator {
            anchors.fill: parent

            enabled: controller.state === "IDLE" || controller.state === "PROGRAM_INVALID"

            onAccept: {
                console.log("Material Reconfigured")
                console.log("xExtent " + xExtent + " zExtent " + zExtent + " height " + h)
                console.log("xGrains " + xGrains + " zGrains " + zGrains )
                console.log("heightThreshold " + heightThreshold)

                material.regenerate(xExtent, zExtent, h, xGrains, zGrains, heightThreshold)

                materialPopup.close()
            }

            onCancel: materialPopup.close()
        }
    }

    Scene3D {
        id: scene3d
        anchors.fill: parent
        anchors.margins: 0
        focus: true
        aspects: ["input", "logic", "render"]
        cameraAspectRatioMode: Scene3D.AutomaticAspectRatio

        Entity {
            id: sceneRoot
            objectName: "sceneRoot"

            Camera {
                id: camera
                projectionType: CameraLens.PerspectiveProjection
                fieldOfView: 45
                nearPlane : 0.1
                farPlane : 1000.0
                position: Qt.vector3d( 0.0, 200.0, 200.0 )
                upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
                viewCenter: Qt.vector3d( 0.0, 0.0, 0.0 )
            }

            OrbitCameraController {
                camera: camera
                linearSpeed: 1000.0
            }

            components: [
                RenderSettings {
                    activeFrameGraph: ForwardRenderer {
                        camera: camera
                        clearColor: "gray"
                    }
                },
                InputSettings { },
//                DirectionalLight {
//                    worldDirection: Qt.vector3d(0.2, -1, 0.1)
//                },

                DirectionalLight {
                    worldDirection: Qt.vector3d(0.5, -1, 1)
                },

                DirectionalLight {
                    worldDirection: Qt.vector3d(-1, -1, -0.8)
                }
            ]

            GoochMaterial {
                id: tableMaterial
            }

            CuboidMesh {
                id: tableMesh
                xExtent: 300
                yExtent: 1
                zExtent: 300
            }

            Entity {
                id: table
                components: [ tableMesh, tableMaterial ]
            }

            MaterialChunk {
                id: material
            }

            ToolPath {
                id: toolPath
            }

        }

    }

    ProgramStepView {
        id: programStepView

        width: 50

        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
            margins: 5
        }

        programModel: program
        toolsetModel: toolset

        onAdd: openFileDialog.open()

    }

    ProgramTimeline {

        anchors {
            left: programStepView.right
            bottom: parent.bottom
            right: parent.right
            margins: 5
        }

        programModel: program

        onPlay: controller.start()
        onPause: material.fullBufferUpdate()
        onReset: controller.reset()
        onRunToEnd: controller.runToEnd()

        state: controller.state

        onMultiplierChanged: controller.speedMultiplier = multiplier

        progress: controller.progress

    }

    ToolController {

        anchors {
            right: parent.right
            top: parent.top
            margins: 5
        }

        toolsetModel: toolset
        currentTool: controller.currentTool

        onDisplayPathsChanged: toolPath.setEnabled(display)

    }

    ProgramModel {
        id: program

        toolset: toolset

        onToolRequested: {
            toolRequestedDialog.type = type
            toolRequestedDialog.diameter = diameter
            toolRequestedDialog.open()
        }
    }

    ToolsetModel {
        id: toolset
        sceneRoot: sceneRoot
    }

    MillingController {
        id: controller

        onMillingError: {
            simulationErrorDialog.informativeText = message
            simulationErrorDialog.open()
        }
    }

    Component.onCompleted: {
        controller.material = material
        controller.toolset = toolset
        controller.program = program
        toolPath.program = program
    }

    Connections {
        target: program

        onLoadError: {
            gCodeErrorDialog.informativeText = message
            gCodeErrorDialog.open()
        }
    }

}
