import QtQuick 2.0
import QtQuick.Controls 2.2

import "controls"

RoundedPlate {
    id: root

    width: 300
    height: 160

    property int stepNumber: 1
    property string fileName: "tool3.k15"
    property real pathLength: 1425.0
    property var tool: null
    property bool enabled: true

    property var toolsetModel

    signal remove()
    signal moveUp()
    signal moveDown()

    signal changeTool(var tool)

    Rectangle {
        id: infoRectangle

        color: "white"

        border {
            width: 1
            color: "#757575"
        }

        radius: 5

        anchors {
            left: parent.left
            right: buttonsColumn.left
            top: parent.top
            bottom: parent.bottom
            margins: 10
        }

        Column {

            spacing: 10

            anchors {
                top: parent.top
                left: parent.left
                margins: 10
            }

            Text {
                text: "Step " + root.stepNumber
                font.bold: true
                font.pointSize: 12
            }

            Row {
                id: row2
                spacing: 5
                Text {
                    text: "Source file:"
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 0
                }
                Text {
                    id: fileNameText
                    text: root.fileName
                    font.pointSize: 12
                }
            }

            Row {
                id: row1
                spacing: 5
                Text {
                    text: "Path length:"
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 0
                }
                Text {
                    id: pathLengthText
                    text: root.pathLength.toLocaleString() + " mm"
                    font.pointSize: 12
                }
            }

            Row {
                id: row
                spacing: 5
                Text {
                    text: "Tool:"
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 0
                }

                ToolComboBox {

                    anchors.verticalCenter: parent.verticalCenter

                    toolsetModel: root.toolsetModel

                    tool: root.tool

                    onChangeTool: root.changeTool(tool)
                }
            }

        }

    }

    Column {
        id: buttonsColumn

        spacing: 5

        anchors {
            right: parent.right
            top: parent.top
            margins: 10
        }

        RoundedImageButton {
            width: 30
            height: 30

            source: "qrc:///images/cross.svg"

            onClicked: root.remove()

            enabled: root.enabled
        }

        RoundedImageButton {
            width: 30
            height: 30

            source: "qrc:///images/up.svg"

            onClicked: root.moveUp()

            enabled: root.enabled
        }

        RoundedImageButton {
            width: 30
            height: 30

            source: "qrc:///images/down.svg"

            onClicked: root.moveDown()

            enabled: root.enabled
        }

    }

}
