import QtQuick 2.4

MaterialConfiguratorForm {

    property bool enabled: true

    signal cancel()
    signal accept(real xExtent, real zExtent, real h, int xGrains, int zGrains, real heightThreshold)

    applyButton.onClicked: accept(xValueSpinBox.realValue, yValueSpinBox.realValue, zValueSpinBox.realValue, xGranSpinBox.value, yGranSpinBox.value, heightThresholdSpinBox.realValue)
    closeButton.onClicked: cancel()

    applyButton.enabled: enabled

    xGrainSizeText.text: Number(xValueSpinBox.realValue / xGranSpinBox.value).toLocaleString(locale, 'f', 2) + " mm"
    yGrainSizeText.text: Number(yValueSpinBox.realValue / yGranSpinBox.value).toLocaleString(locale, 'f', 2) + " mm"

}
