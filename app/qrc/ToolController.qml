import QtQuick 2.7
import QtQuick.Controls 2.2

import "controls"

RoundedPlate {
    id: root

    width: 220
    height: 90

    property var toolsetModel: null
    property var currentTool: null

    signal displayPathsChanged(bool display)

    function toolToString(tool) {
        if (tool === null) {
            return "None"
        } else {
            return tool.type + " Ø" + tool.diameter
        }
    }

    Grid {
        columns: 2
        spacing: 10

        anchors {
            fill: parent
            margins: 10
        }

        horizontalItemAlignment: Grid.AlignLeft
        verticalItemAlignment: Grid.AlignVCenter

        Text {
            text: "Current Tool"
        }

        Text {
            id: toolText
            text: root.toolToString(root.currentTool)
        }

        Text {
            text: "Display paths"
        }

        CheckBox {
            id: displayPathsCheckBox

            onCheckedChanged: root.displayPathsChanged(checked)

            checked: true
        }

    }

}
