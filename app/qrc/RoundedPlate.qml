import QtQuick 2.0

Rectangle {
    id: root

    default property alias content: content.children

    width: 300
    height: 200

    color: "#FAFAFA"

    border {
        width: 1
        color: "#757575"
    }

    radius: 5

    Item {
        id: content

        anchors.fill: parent
        anchors.margins: 5
    }

}