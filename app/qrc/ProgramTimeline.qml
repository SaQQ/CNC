import QtQuick 2.0
import QtQuick.Controls 2.2

import "controls"

RoundedPlate {
    id: root

    width: 700
    height: 50
    radius: 4

    property var programModel

    property real progress: 0.0

    signal play()
    signal pause()
    signal reset()
    signal runToEnd()
    signal multiplierChanged(real multiplier)


    Row {
        id: controlRow

        spacing: 5

        anchors {
            right: parent.right
            rightMargin: 10
            verticalCenter: parent.verticalCenter
        }

        Item {
            id: item1
            width: 200
            height: 35
            anchors.verticalCenter: parent.verticalCenter

            Slider {
                id: multiplierSlider
                font.pointSize: 7
                anchors.top: multiplierLabel.bottom
                anchors.topMargin: 5
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.bottom: parent.bottom
                from: -3
                to: 3
                stepSize: 1
                snapMode: Slider.SnapAlways

                onValueChanged: root.multiplierChanged(Math.pow(2, value))
            }

            Text {
                id: multiplierLabel
                text: qsTr("Speed multiplier: x") + Math.pow(2, multiplierSlider.value)
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
                font.pixelSize: 9
            }
        }

        RoundedImageButton {
            id: startButton
            width: 30
            height: 30
            anchors.verticalCenter: parent.verticalCenter

            source: "qrc:///images/play.svg"

            onClicked: root.play()
        }


        RoundedImageButton {
            id: pauseButton
            width: 30
            height: 30
            anchors.verticalCenter: parent.verticalCenter

            source: "qrc:///images/pause.svg"

            onClicked: root.pause()

            enabled: false
        }


        RoundedImageButton {
            id: resetButton
            width: 30
            height: 30
            anchors.verticalCenter: parent.verticalCenter

            source: "qrc:///images/rewind.svg"

            onClicked: root.reset()
        }

        RoundedImageButton {
            id: runToEndButton
            width: 30
            height: 30
            anchors.verticalCenter: parent.verticalCenter

            source: "qrc:///images/forward.svg"

            onClicked: root.runToEnd()
        }


    }

    Text {
        id: statusText
        y: 20
        text: qsTr("Status")
        horizontalAlignment: Text.AlignHCenter
        anchors.right: controlRow.left
        anchors.rightMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.verticalCenter: parent.verticalCenter
        font.pixelSize: 12
    }
    states: [
        State {
            name: "IDLE"

            PropertyChanges {
                target: bar
                visible: false
            }

            PropertyChanges {
                target: statusText
                text: qsTr("Program valid. Ready to start simulation.")
            }

            PropertyChanges {
                target: resetButton
                enabled: false
            }

        },
        State {
            name: "RUNNING"

            PropertyChanges {
                target: statusText
                visible: false
            }

            PropertyChanges {
                target: startButton
                enabled: false
            }

        },
        State {
            name: "RUNNING_TO_END"

            PropertyChanges {
                target: statusText
                visible: false
            }

            PropertyChanges {
                target: startButton
                enabled: false
            }

            PropertyChanges {
                target: resetButton
                enabled: false
            }

            PropertyChanges {
                target: runToEndButton
                enabled: false
            }
        },
        State {
            name: "FINISHED"

            PropertyChanges {
                target: statusText
                visible: false
            }

            PropertyChanges {
                target: startButton
                enabled: false
            }

            PropertyChanges {
                target: runToEndButton
                enabled: false
            }
        },
        State {
            name: "PROGRAM_INVALID"

            PropertyChanges {
                target: bar
                visible: false
            }

            PropertyChanges {
                target: statusText
                text: qsTr("Program is invalid.")
            }

            PropertyChanges {
                target: startButton
                enabled: false
            }

            PropertyChanges {
                target: resetButton
                enabled: false
            }

            PropertyChanges {
                target: runToEndButton
                enabled: false
            }
        }
    ]

    Item {
        id: bar

        height: 10
        visible: true

        anchors {
            left: parent.left
            leftMargin: 10
            right: controlRow.left
            rightMargin: 10
            verticalCenter: parent.verticalCenter
        }

        Row {

            Repeater {
                model: root.programModel
                delegate: Component {
                    Rectangle {
                        color: index % 2 === 1 ? "#2196F3" : "#0D47A1"
                        height: bar.height
                        width: bar.width * pathLength / root.programModel.totalPathLength
                        radius: 3
                    }
                }
            }

        }

        Rectangle {
            id: indicator

            x: bar.width * root.progress / (root.programModel ? root.programModel.totalPathLength : 1) - width / 2
            y: -5

            width: 10
            height: 20

            color: "white"

            border {
                width: 1
                color: "#757575"
            }

            radius: 3

        }

    }
}
