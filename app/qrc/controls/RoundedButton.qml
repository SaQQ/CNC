import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: root

    height: 40
    width: 40

    property string text: ""
    property color color: "green"
    property bool enabled: true

    signal clicked()

    Rectangle {
        id: background

        anchors.fill: parent
        anchors.margins: root.enabled && clickArea.containsMouse ? -1 : 0

        MouseArea {
            id: clickArea

            anchors {
                fill: parent
                margins: 0
            }

            hoverEnabled: true

            onClicked: if(root.enabled) root.clicked()
        }

        Text {
            anchors.centerIn: parent
            text: root.text
            color: root.enabled ? "#212121" : "#757575"
        }

        color: root.enabled ? (clickArea.pressed ? Qt.lighter(root.color, 1.2) : root.color) : "#BDBDBD"

        border {
            color: "#757575"
            width: 1
        }

        radius: 3
    }

}

