import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: root

    height: 40
    width: 40

    property url source

    property bool enabled: true

    signal clicked()

    Rectangle {

        anchors.fill: parent
        anchors.margins: root.enabled && clickArea.containsMouse ? -1 : 0

        MouseArea {
            id: clickArea

            anchors {
                fill: parent
                margins: 0
            }

            hoverEnabled: true

            onClicked: if(root.enabled) root.clicked()
        }

        Image {
            id: image
            anchors.fill: parent
            anchors.margins: 1
            source: root.source
            sourceSize.width: width
            sourceSize.height: height
        }

        BrightnessContrast {
            visible: root.enabled
            anchors.fill: image
            source: image
            brightness: clickArea.pressed ? 0.5 : 0.0
        }

        Desaturate {
            visible: !root.enabled
            anchors.fill: image
            source: image
            desaturation: 1.0
        }

        border {
            color: "#757575"
            width: 1
        }

        radius: 3
    }

}

