import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

import "controls"


ToolsetEditorForm {
    id: editorForm

    property var model

    property var editingEnabled: model ? !model.locked: true

    width: 1000

    addToolButton.onClicked: {
        model.insertRows(0, 1)
    }

    addToolButton.enabled: editingEnabled

    toolsListView.model: model

    toolsListView.delegate: Component {
        id: toolDelegate

        Item {
            id: root

            height: 60

            anchors {
                left: parent.left
                right: parent.right
            }

            RowLayout {
                id: row

                anchors.fill: parent

                spacing: 10

                Item {
                    id: typeSection

                    Layout.fillWidth: true

                    height: row.height

                    Text {
                        id: typeLabel
                        anchors.top: typeSection.top
                        text: "Type"
                    }

                    ComboBox {
                        id: typeComboBox
                        anchors.topMargin: 4
                        anchors.top: typeLabel.bottom
                        anchors.bottom: typeSection.bottom
                        width: typeSection.width

                        currentIndex: model.findIndex(function(t) {
                            return t === type
                        })

                        onCurrentIndexChanged: {
                            type = model[currentIndex]
                        }

                        model: ["flat", "spherical"]

                        enabled: editorForm.editingEnabled
                    }

                }

                Item {
                    id: diameterSection

                    Layout.fillWidth: true

                    height: row.height

                    Text {
                        id: diameterLabel
                        anchors.top: diameterSection.top
                        text: "Diameter"
                    }

                    TextField {
                        anchors.top: diameterLabel.bottom
                        anchors.topMargin: 4
                        anchors.bottom: diameterSection.bottom
                        width: diameterSection.width
                        text: model.diameter

                        onTextChanged: {
                            diameter = Number(text)
                        }

                        enabled: editorForm.editingEnabled
                    }

                }

                Item {
                    id: cutLengthSection

                    Layout.fillWidth: true

                    height: row.height

                    Text {
                        id: cutLengthLabel
                        anchors.top: cutLengthSection.top
                        text: "Cut Length"
                    }

                    TextField {
                        anchors.top: cutLengthLabel.bottom
                        anchors.topMargin: 4
                        anchors.bottom: cutLengthSection.bottom
                        width: cutLengthSection.width
                        text: model.cutLength

                        onTextChanged: {
                            cutLength = Number(text)
                        }

                        enabled: editorForm.editingEnabled
                    }

                }

                Item {
                    id: lengthSection

                    Layout.fillWidth: true

                    height: row.height

                    Text {
                        id: lengthLabel
                        anchors.top: lengthSection.top
                        text: "Length"
                    }

                    TextField {
                        anchors.top: lengthLabel.bottom
                        anchors.topMargin: 4
                        anchors.bottom: lengthSection.bottom
                        width: lengthSection.width
                        text: model.length

                        onTextChanged: {
                            model.length = Number(text)
                        }

                        enabled: editorForm.editingEnabled
                    }

                }

                RoundedButton {
                    //TODO: Disable button
                    id: removeButton

                    height: 30
                    width: 60

                    color: "#F44336"

                    text: "Remove"

                    onClicked: {
                        root.ListView.view.model.removeRows(index, 1)
                    }

                    enabled: editorForm.editingEnabled
                }

            }
        }
    }

}
