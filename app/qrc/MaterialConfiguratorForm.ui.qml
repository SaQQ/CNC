import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "controls"

Item {
    id: root
    width: 600
    height: 400
    property alias yGrainSizeText: yGrainSizeText
    property alias xGrainSizeText: xGrainSizeText
    property alias applyButton: applyButton
    property alias closeButton: closeButton
    property alias yGranSpinBox: yGranSpinBox
    property alias xGranSpinBox: xGranSpinBox
    property alias zValueSpinBox: zValueSpinBox
    property alias yValueSpinBox: yValueSpinBox
    property alias xValueSpinBox: xValueSpinBox
    property alias heightThresholdSpinBox: heightThresholdSpinBox

    GroupBox {
        id: dimGroupBox
        height: 250
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        title: qsTr("Dimmensions")

        GridLayout {
            id: gridLayout
            rows: 4
            columns: 4
            anchors.fill: parent

            Text {
                id: text5
                text: qsTr("")
                visible: true
                font.pixelSize: 12
            }

            Text {
                id: text1
                text: qsTr("Value")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                font.pixelSize: 12
            }

            Text {
                id: text2
                text: qsTr("Granularity")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                font.pixelSize: 12
            }

            Text {
                id: text10
                text: qsTr("Grain Size")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                font.pixelSize: 12
            }

            Text {
                id: text3
                text: qsTr("Width [X]")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                font.pixelSize: 12
            }

            DoubleSpinBox {
                id: xValueSpinBox
                value: 1500
                from: 1
                to: 200
                Layout.fillWidth: true
            }

            SpinBox {
                id: xGranSpinBox
                to: 1000
                from: 20
                value: 600
                Layout.fillWidth: true
                editable: true
            }

            Text {
                id: xGrainSizeText
                text: "10 mm"
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                font.pixelSize: 12
            }

            Text {
                id: text4
                text: qsTr("Length [Y]")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                font.pixelSize: 12
            }

            DoubleSpinBox {
                id: yValueSpinBox
                value: 1500
                from: 1
                to: 200
                Layout.fillWidth: true
            }

            SpinBox {
                id: yGranSpinBox
                to: 1000
                from: 20
                value: 600
                Layout.fillWidth: true
                editable: true
            }

            Text {
                id: yGrainSizeText
                text: qsTr("10 mm")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                font.pixelSize: 12
            }

            Text {
                id: text6
                text: qsTr("Height [Z]")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                font.pixelSize: 12
            }

            DoubleSpinBox {
                id: zValueSpinBox
                value: 500
                from: 1
                Layout.columnSpan: 3
                to: 100
                Layout.fillWidth: true
            }

            Text {
                            text: qsTr("Height Threshold")
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                            font.pixelSize: 12
                        }

                        DoubleSpinBox {
                            id: heightThresholdSpinBox
                            value: 200
                            from: 1
                            Layout.columnSpan: 3
                            to: 100
                            Layout.fillWidth: true
                        }
        }
    }

    Row {
        id: row
        width: 200
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 6
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 6

        Button {
            id: closeButton
            text: qsTr("Close")
        }

        Button {
            id: applyButton
            text: qsTr("Apply")
        }
    }
}
