import QtQuick 2.0
import QtQuick.Controls 2.2

Item {
    id: root

    width: 160
    height: 25

    property var toolsetModel

    property bool enabled: true

    property var tool: null

    signal changeTool(var tool)

    ComboBox {
        id: comboBox

        anchors.fill: parent

        model: root.toolsetModel

        enabled: root.enabled

        delegate: ItemDelegate {
            width: comboBox.width
            text: comboBox.textFromParams(type, diameter, cutLength)
        }

        contentItem: Text {
            text: {
                // TODO: Evaluate it on data change?
                if (comboBox.currentIndex === -1)
                    return ""
                var index = comboBox.model.index(comboBox.currentIndex, 0)
                return comboBox.textFromParams(comboBox.model.data(index, 257), comboBox.model.data(index, 258), comboBox.model.data(index, 259))
            }

            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
        }

        currentIndex: model ? model.findIndex(root.tool) : -1

        onCurrentIndexChanged: root.changeTool(root.toolsetModel.getTool(currentIndex))

        function textFromParams(type, diameter, cutLength) {
            return type + " Ø" + diameter + " " + cutLength + " mm"
        }
    }

}