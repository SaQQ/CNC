// Created by Paweł Sobótka on 16.08.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef MARCHING_CUBES_MATRIX_HPP
#define MARCHING_CUBES_MATRIX_HPP

#include "vector.hpp"

namespace maths {

    template<typename T, std::size_t rows, std::size_t cols>
    class mat {
        using row_type = vec<T, cols>;
        using arr_type = std::array<row_type, rows>;
    public:
        mat<T, rows, cols>() {

        }

        mat<T, rows, cols>(const arr_type &arr) : data{arr} {

        }

        row_type &operator[](std::size_t row) { return data[row]; }

        const row_type &operator[](std::size_t row) const { return data[row]; }

        template<std::size_t other_cols>
        mat<T, rows, other_cols> operator*(const mat<T, cols, other_cols> &rhs) const {
            mat<T, rows, other_cols> ret;
            for (std::size_t r = 0; r < rows; ++r) {
                for (std::size_t c = 0; c < other_cols; ++c) {
                    ret.data[r][c] = data[r][0] * rhs.data[0][c];
                    for (std::size_t i = 1; i < cols; ++i)
                        ret.data[r][c] += data[r][i] * rhs.data[i][c];
                }
            }
            return ret;
        };

        vec<T, rows> operator*(const vec<T, cols> &rhs) const {
            vec<T, rows> ret;
            for (std::size_t r = 0; r < rows; ++r)
                ret[r] = data[r] * rhs;
            return ret;
        }

        mat<T, cols, rows> tr() const {
            mat<T, cols, rows> ret;
            for (std::size_t r = 0; r < rows; ++r)
                for (std::size_t c = 0; c < cols; ++c)
                    ret.data[c][r] = data[r][c];
            return ret;
        }

        mat<T, cols, rows>& operator+=(const mat<T, cols, rows> &rhs) {
            for (std::size_t r = 0; r < rows; ++r)
                data[r] += rhs.data[r];
        };

        mat<T, cols, rows>& operator-=(const mat<T, cols, rows> &rhs) {
            for (std::size_t r = 0; r < rows; ++r)
                data[r] -= rhs.data[r];
        };

        mat<T, cols, rows>& operator*=(const T &rhs) {
            for (std::size_t r = 0; r < rows; ++r)
                data[r] *= rhs;
        };

        mat<T, cols, rows>& operator/=(const T &rhs) {
            for (std::size_t r = 0; r < rows; ++r)
                data[r] /= rhs;
        };

        mat<T, cols, rows>& operator+(const mat<T, cols, rows> &rhs) const {
            mat<T, cols, rows> ret = *this;
            ret += rhs;
            return ret;
        };

        mat<T, cols, rows>& operator-(const mat<T, cols, rows> &rhs) const {
            mat<T, cols, rows> ret = *this;
            ret -= rhs;
            return ret;
        };

        mat<T, cols, rows>& operator*(const T &rhs) const {
            mat<T, cols, rows> ret = *this;
            ret *= rhs;
            return ret;
        };

        mat<T, cols, rows>& operator/(const T &rhs) const {
            mat<T, cols, rows> ret = *this;
            ret /= rhs;
            return ret;
        };

        /* Inverse matrix functions for matrices 1x1, 2x2, 3x3 and 4x4 */

        template<std::size_t r = rows, std::size_t c = cols>
        typename std::enable_if<r == 1 && c == 1, mat<T, r, c>>::type inv() const {
            return mat<T, r, c>{{vec<T, r>{{1 / data[0][0]}}}};
        }

        template<std::size_t r = rows, std::size_t c = cols>
        typename std::enable_if<r == 2 && c == 2, mat<T, r, c>>::type inv() const {
            auto det = data[0][0] * data[1][1] - data[0][1] * data[1][0];
            return mat<T, r, c>{{
                                        vec<T, r>{{data[1][1] / det, -data[0][1] / det}},
                                        vec<T, r>{{-data[1][0] / det, data[0][0] / det}}
                                }};
        }

        template<std::size_t r = rows, std::size_t c = cols>
        typename std::enable_if<r == 3 && c == 3, mat<T, r, c>>::type inv() const {
            auto det = data[0][0] * data[1][1] * data[2][2]
                       + data[1][0] * data[2][1] * data[0][2]
                       + data[2][0] * data[0][1] * data[1][2]
                       - data[0][0] * data[2][1] * data[1][2]
                       - data[2][0] * data[1][1] * data[0][2]
                       - data[1][0] * data[0][1] * data[2][2];

            return mat<T, r, c>{{
                                        vec<T, r>{{
                                                          (data[1][1] * data[2][2] - data[1][2] * data[2][1]) / det,
                                                          (data[0][2] * data[2][1] - data[0][1] * data[2][2]) / det,
                                                          (data[0][1] * data[1][2] - data[0][2] * data[1][1]) / det
                                                  }},
                                        vec<T, r>{{
                                                          (data[1][2] * data[2][0] - data[1][0] * data[2][2]) / det,
                                                          (data[0][0] * data[2][2] - data[0][2] * data[2][0]) / det,
                                                          (data[0][2] * data[1][0] - data[0][0] * data[1][2]) / det
                                                  }},
                                        vec<T, r>{{
                                                          (data[1][0] * data[2][1] - data[1][1] * data[2][0]) / det,
                                                          (data[0][1] * data[2][0] - data[0][0] * data[2][1]) / det,
                                                          (data[0][0] * data[1][1] - data[0][1] * data[1][0]) / det
                                                  }}
                                }};
        }

        template<std::size_t r = rows, std::size_t c = cols>
        typename std::enable_if<r == 4 && c == 4, mat<T, r, c>>::type inv() const {
            mat<T, r, c> inv;
            inv.data[0][0] = data[1][1] * data[2][2] * data[3][3] -
                             data[1][1] * data[3][2] * data[2][3] -
                             data[1][2] * data[2][1] * data[3][3] +
                             data[1][2] * data[3][1] * data[2][3] +
                             data[1][3] * data[2][1] * data[3][2] -
                             data[1][3] * data[3][1] * data[2][2];

            inv.data[0][1] = -data[0][1] * data[2][2] * data[3][3] +
                             data[0][1] * data[3][2] * data[2][3] +
                             data[0][2] * data[2][1] * data[3][3] -
                             data[0][2] * data[3][1] * data[2][3] -
                             data[0][3] * data[2][1] * data[3][2] +
                             data[0][3] * data[3][1] * data[2][2];

            inv.data[0][2] = data[0][1] * data[1][2] * data[3][3] -
                             data[0][1] * data[3][2] * data[1][3] -
                             data[0][2] * data[1][1] * data[3][3] +
                             data[0][2] * data[3][1] * data[1][3] +
                             data[0][3] * data[1][1] * data[3][2] -
                             data[0][3] * data[3][1] * data[1][2];

            inv.data[0][3] = -data[0][1] * data[1][2] * data[2][3] +
                             data[0][1] * data[2][2] * data[1][3] +
                             data[0][2] * data[1][1] * data[2][3] -
                             data[0][2] * data[2][1] * data[1][3] -
                             data[0][3] * data[1][1] * data[2][2] +
                             data[0][3] * data[2][1] * data[1][2];

            inv.data[1][0] = -data[1][0] * data[2][2] * data[3][3] +
                             data[1][0] * data[3][2] * data[2][3] +
                             data[1][2] * data[2][0] * data[3][3] -
                             data[1][2] * data[3][0] * data[2][3] -
                             data[1][3] * data[2][0] * data[3][2] +
                             data[1][3] * data[3][0] * data[2][2];

            inv.data[1][1] = data[0][0] * data[2][2] * data[3][3] -
                             data[0][0] * data[3][2] * data[2][3] -
                             data[0][2] * data[2][0] * data[3][3] +
                             data[0][2] * data[3][0] * data[2][3] +
                             data[0][3] * data[2][0] * data[3][2] -
                             data[0][3] * data[3][0] * data[2][2];

            inv.data[1][2] = -data[0][0] * data[1][2] * data[3][3] +
                             data[0][0] * data[3][2] * data[1][3] +
                             data[0][2] * data[1][0] * data[3][3] -
                             data[0][2] * data[3][0] * data[1][3] -
                             data[0][3] * data[1][0] * data[3][2] +
                             data[0][3] * data[3][0] * data[1][2];

            inv.data[1][3] = data[0][0] * data[1][2] * data[2][3] -
                             data[0][0] * data[2][2] * data[1][3] -
                             data[0][2] * data[1][0] * data[2][3] +
                             data[0][2] * data[2][0] * data[1][3] +
                             data[0][3] * data[1][0] * data[2][2] -
                             data[0][3] * data[2][0] * data[1][2];

            inv.data[2][0] = data[1][0] * data[2][1] * data[3][3] -
                             data[1][0] * data[3][1] * data[2][3] -
                             data[1][1] * data[2][0] * data[3][3] +
                             data[1][1] * data[3][0] * data[2][3] +
                             data[1][3] * data[2][0] * data[3][1] -
                             data[1][3] * data[3][0] * data[2][1];

            inv.data[2][1] = -data[0][0] * data[2][1] * data[3][3] +
                             data[0][0] * data[3][1] * data[2][3] +
                             data[0][1] * data[2][0] * data[3][3] -
                             data[0][1] * data[3][0] * data[2][3] -
                             data[0][3] * data[2][0] * data[3][1] +
                             data[0][3] * data[3][0] * data[2][1];

            inv.data[2][2] = data[0][0] * data[1][1] * data[3][3] -
                             data[0][0] * data[3][1] * data[1][3] -
                             data[0][1] * data[1][0] * data[3][3] +
                             data[0][1] * data[3][0] * data[1][3] +
                             data[0][3] * data[1][0] * data[3][1] -
                             data[0][3] * data[3][0] * data[1][1];

            inv.data[2][3] = -data[0][0] * data[1][1] * data[2][3] +
                             data[0][0] * data[2][1] * data[1][3] +
                             data[0][1] * data[1][0] * data[2][3] -
                             data[0][1] * data[2][0] * data[1][3] -
                             data[0][3] * data[1][0] * data[2][1] +
                             data[0][3] * data[2][0] * data[1][1];

            inv.data[3][0] = -data[1][0] * data[2][1] * data[3][2] +
                             data[1][0] * data[3][1] * data[2][2] +
                             data[1][1] * data[2][0] * data[3][2] -
                             data[1][1] * data[3][0] * data[2][2] -
                             data[1][2] * data[2][0] * data[3][1] +
                             data[1][2] * data[3][0] * data[2][1];

            inv.data[3][1] = data[0][0] * data[2][1] * data[3][2] -
                             data[0][0] * data[3][1] * data[2][2] -
                             data[0][1] * data[2][0] * data[3][2] +
                             data[0][1] * data[3][0] * data[2][2] +
                             data[0][2] * data[2][0] * data[3][1] -
                             data[0][2] * data[3][0] * data[2][1];

            inv.data[3][2] = -data[0][0] * data[1][1] * data[3][2] +
                             data[0][0] * data[3][1] * data[1][2] +
                             data[0][1] * data[1][0] * data[3][2] -
                             data[0][1] * data[3][0] * data[1][2] -
                             data[0][2] * data[1][0] * data[3][1] +
                             data[0][2] * data[3][0] * data[1][1];

            inv.data[3][3] = data[0][0] * data[1][1] * data[2][2] -
                             data[0][0] * data[2][1] * data[1][2] -
                             data[0][1] * data[1][0] * data[2][2] +
                             data[0][1] * data[2][0] * data[1][2] +
                             data[0][2] * data[1][0] * data[2][1] -
                             data[0][2] * data[2][0] * data[1][1];

            double det = data[0][0] * inv.data[0][0] + data[1][0] * inv.data[0][1] + data[2][0] * inv.data[0][2] + data[3][0] * inv.data[0][3];

            return inv / det;
        }

        /* Inverse matrix function for matrices bigger than 4x4 using LU decomposition */

        template<std::size_t r = rows, std::size_t c = cols>
        typename std::enable_if<r == c && r >= 5, mat<T, r, c>>::type inv() const {
            throw std::logic_error{"Not implemented"};
        }

    private:
        arr_type data;
    };

    template<typename T, std::size_t rows, std::size_t cols>
    mat<T, rows, cols> operator*(T lhs, const mat<T, rows, cols> &rhs) {
        const mat<T, rows, cols> ret = rhs;
        ret *= lhs;
        return ret;
    };

    using mat2x2 = mat<float, 2, 2>;
    using mat3x3 = mat<float, 3, 3>;
    using mat4x4 = mat<float, 4, 4>;
    using mat3x4 = mat<float, 3, 4>;
    using mat4x3 = mat<float, 4, 3>;

}

#endif //MARCHING_CUBES_MATRIX_HPP
