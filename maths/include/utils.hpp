// Created by Paweł Sobótka on 26.10.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_UTILS_HPP
#define CNC_SIMULATOR_UTILS_HPP

#include "vector.hpp"

namespace maths {

    vec3 orthogonal_vector(const vec3& v);

}

#endif //CNC_SIMULATOR_UTILS_HPP
