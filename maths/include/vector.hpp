// Created by Paweł Sobótka on 16.08.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef MARCHING_CUBES_VECTOR_HPP
#define MARCHING_CUBES_VECTOR_HPP

#include <array>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <iosfwd>

namespace maths {

    template<typename T, std::size_t len>
    class vec {
        using arr_type = std::array<T, len>;
    public:

        vec<T, len>() {

        }

        vec<T, len>(const arr_type& arr) : data{arr} {

        }

        T& operator[](std::size_t idx) { return data[idx]; }
        const T& operator[](std::size_t idx) const { return data[idx]; }

        template<std::size_t l = len> typename std::enable_if<l >= 1, T>::type &x() { return data[0]; };
        template<std::size_t l = len> typename std::enable_if<l >= 2, T>::type &y() { return data[1]; };
        template<std::size_t l = len> typename std::enable_if<l >= 3, T>::type &z() { return data[2]; };
        template<std::size_t l = len> typename std::enable_if<l >= 4, T>::type &w() { return data[3]; };

        template<std::size_t l = len> const typename std::enable_if<l >= 1, T>::type &x() const { return data[0]; };
        template<std::size_t l = len> const typename std::enable_if<l >= 2, T>::type &y() const { return data[1]; };
        template<std::size_t l = len> const typename std::enable_if<l >= 3, T>::type &z() const { return data[2]; };
        template<std::size_t l = len> const typename std::enable_if<l >= 4, T>::type &w() const { return data[3]; };

        template<std::size_t l = len> typename std::enable_if<l >= 1, T>::type &r() { return data[0]; };
        template<std::size_t l = len> typename std::enable_if<l >= 2, T>::type &g() { return data[1]; };
        template<std::size_t l = len> typename std::enable_if<l >= 3, T>::type &b() { return data[2]; };
        template<std::size_t l = len> typename std::enable_if<l >= 4, T>::type &a() { return data[3]; };

        vec<T, len>& operator+=(const vec<T, len>& rhs) {
            for(std::size_t i = 0; i < len; ++i)
                data[i] += rhs.data[i];
            return *this;
        };

        vec<T, len>& operator-=(const vec<T, len>& rhs) {
            for(std::size_t i = 0; i < len; ++i)
                data[i] -= rhs.data[i];
            return *this;
        };

        vec<T, len>& operator*=(T rhs) {
            for(std::size_t i = 0; i < len; ++i)
                data[i] *= rhs;
            return *this;
        };

        vec<T, len>& operator/=(T rhs) {
            for(std::size_t i = 0; i < len; ++i)
                data[i] /= rhs;
            return *this;
        };

        vec<T, len> operator+(const vec<T, len>& rhs) const {
            vec<T, len> ret = *this;
            ret += rhs;
            return ret;
        };

        vec<T, len> operator-(const vec<T, len>& rhs) const {
            vec<T, len> ret = *this;
            ret -= rhs;
            return ret;
        };

        vec<T, len> operator*(const T& rhs) const {
            vec<T, len> ret = *this;
            ret *= rhs;
            return ret;
        };

        vec<T, len> operator/(const T& rhs) const {
            vec<T, len> ret = *this;
            ret /= rhs;
            return ret;
        };

        /* Vector dot product */

        T operator*(const vec<T, len>& rhs) const {
            T acc = data[0] * rhs.data[0];
            for(std::size_t i = 1; i < len; ++i)
                acc += data[i] * rhs.data[i];
            return acc;
        }

        /* Normalization */

        T length_squared() const {
            return std::accumulate(data.begin() + 1, data.end(), data[0] * data[0], [](T acc, T curr){ return acc + curr * curr; });
        }

        T length() const {
            return std::sqrt(length_squared());
        }

        void normalize() {
            auto l = length();
            for(std::size_t i = 0; i < len; ++i)
                data[i] /= l;
        }

        vec<T, len> normalized() const {
            auto ret = *this;
            ret.normalize();
            return ret;
        };

    private:
        arr_type data;
    };

    template<typename T, std::size_t len>
    vec<T, len> operator*(T lhs, const vec<T, len> &rhs) {
        vec<T, len> ret = rhs;
        ret *= lhs;
        return ret;
    };

    /* Vector cross product */

    template<typename T>
    vec<T, 3> operator^(const vec<T, 3> &lhs, const vec<T, 3> &rhs) {
        const auto& u = lhs;
        const auto& v = rhs;
        return vec<T, 3>{{
                                 u[1] * v[2] - u[2] * v[1],
                                 u[2] * v[0] - u[0] * v[2],
                                 u[0] * v[1] - u[1] * v[0]
                         }};
    };

    template<typename T>
    vec<T, 3> cross(const vec<T, 3> &u, const vec<T, 3> &v) {
        return u ^ v;
    };

    using vec2 = vec<float, 2>;
    using vec3 = vec<float, 3>;
    using vec4 = vec<float, 4>;

}

#endif //MARCHING_CUBES_VECTOR_HPP