// Created by Paweł Sobótka on 26.10.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_INTERSECTIONS_HPP
#define CNC_SIMULATOR_INTERSECTIONS_HPP

#include <tuple>

#include <matrix.hpp>
#include <utils.hpp>

namespace maths {

    struct ray {
        vec3 origin;
        vec3 direction;
    };

    struct sphere {
        vec3 center;
        float radius;
    };

    struct cyllinder {
        vec3 start;
        vec3 end;
        float radius;
    };

    struct capsule {
        vec3 start;
        vec3 end;
        float radius;
    };

    struct flat_capsule {
        vec3 start;
        vec3 end;
        float radius;
    };

    template<typename T>
    struct intersection_cache;

    template<>
    struct intersection_cache<sphere> {
        intersection_cache(const sphere& s) {

        }
    };

    template<>
    struct intersection_cache<cyllinder> {
        intersection_cache(const cyllinder &c) {
            vec3 bZ = c.end - c.start;
            vec3 bX = orthogonal_vector(bZ);
            vec3 bY = cross(bZ, bX);

            bX.normalize();
            bY.normalize();

            bX *= c.radius;
            bY *= c.radius;

            auto transform = mat3x3{{ bX, bY, bZ }}.tr();
            transformInv = transform.inv();
        }

        mat3x3 transformInv;
    };

    template<>
    struct intersection_cache<capsule> {
        intersection_cache(const capsule &c) : start_sphere_cache{{c.start, c.radius}}, end_sphere_cache{{c.end, c.radius}}, cyllinder_cache{{c.start, c.end, c.radius}} {
        }

        intersection_cache<sphere> start_sphere_cache;
        intersection_cache<sphere> end_sphere_cache;
        intersection_cache<cyllinder> cyllinder_cache;
    };

    template<>
    struct intersection_cache<flat_capsule> {
        intersection_cache(const flat_capsule& c) {

        }
    };

    std::tuple<bool, float, float> ray_sphere_intersection(const ray& r, const sphere &s, const intersection_cache<sphere> &cache);

    std::tuple<bool, float, float> ray_sphere_intersection(const ray& r, const sphere &s);

    std::tuple<bool, float, float> ray_cyllinder_intersection(const ray &r, const cyllinder &c, const intersection_cache<cyllinder> &cache);

    std::tuple<bool, float, float> ray_cyllinder_intersection(const ray &r, const cyllinder &c);

    std::tuple<bool, float, float> ray_capsule_intersection(const ray& r, const capsule& c, const intersection_cache<capsule> &cache);

    std::tuple<bool, float, float> ray_capsule_intersection(const ray& r, const capsule& c);

    std::tuple<bool, float> ray_flat_capsule_intersection(const vec3 &origin, const flat_capsule &c, const intersection_cache <flat_capsule> &cache);

    std::tuple<bool, float> ray_flat_capsule_intersection(const vec3 &origin, const flat_capsule& c);

    struct ray2d {
        vec2 origin;
        vec2 direction;
    };

    struct circle2d {
        vec2 center;
        float radius;
    };

    template<>
    struct intersection_cache<circle2d> {
        intersection_cache(const circle2d &c) {}
    };

    std::tuple<bool, float, float> ray_circle_intersection_2d(const ray2d &r, const circle2d &c, const intersection_cache<circle2d> &cache);

    std::tuple<bool, float, float> ray_circle_intersection_2d(const ray2d &r, const circle2d &c);

}

#endif //CNC_SIMULATOR_INTERSECTIONS_HPP
