// Created by Paweł Sobótka on 26.10.17.
// Rail-Mil Computers Sp. z o.o.

#include "intersections.hpp"

#include <set>

#include "matrix.hpp"
#include "utils.hpp"

namespace maths {

    std::tuple<bool, float, float> ray_sphere_intersection(const ray &r, const sphere &s, const intersection_cache<sphere> &cache) {
        vec3 l = s.center - r.origin;
        float tca = l * r.direction;

        auto d2 = l * l - tca * tca;
        if(d2 > s.radius * s.radius)
            return {false, 0.0f, 0.0f};

        auto thc = static_cast<float>(std::sqrt(s.radius * s.radius -  d2));

        auto t1 = tca - thc;
        auto t2 = tca + thc;

        if(t1 > t2)
            std::swap(t1, t2);

        if(t1 < 0.0f) {
            t1 = t2;
            if(t1 < 0.0f)
                return {false, 0.0f, 0.0f};
        }

        return {true, t1, t2};
    }

    std::tuple<bool, float, float> ray_sphere_intersection(const ray &r, const sphere &s) {
        return ray_sphere_intersection(r, s, intersection_cache<sphere>(s));
    }

    std::tuple<bool, float, float> ray_cyllinder_intersection(const ray &r, const cyllinder &c, const intersection_cache<cyllinder> &cache) {

        maths::vec3 origin = cache.transformInv * (r.origin - c.start);
        maths::vec3 direction = cache.transformInv * r.direction;

        auto A = direction.x() * direction.x() + direction.y() * direction.y();
        auto B = 2.0f * (origin.x() * direction.x() + origin.y() * direction.y());
        auto C = origin.x() * origin.x() + origin.y() * origin.y() - 1.0f;

        auto det = B * B - 4.0f * A * C;
        if (det < 0.0f)
            return {false, 0.0f, 0.0f};

        auto detSqrt = static_cast<float>(std::sqrt(det));
        auto aInv = 0.5f / A;

        auto t1 = (-B + detSqrt) * aInv;
        auto t2 = (-B - detSqrt) * aInv;

        if (t1 > t2)
            std::swap(t1, t2);

        if (t1 < 0.0f)
            t1 = t2;

        if (t1 < 0.0f)
            return {false, 0.0f, 0.0f};

        auto z1 = origin.z() + t1 * direction.z();
        auto z2 = origin.z() + t2 * direction.z();

        if (z1 < 0.0f || z1 > 1.0f) {

            if (z2 < 0.0f || z2 > 1.0f)
                return {false, 0.0f, 0.0f};

            return {true, t2, t2};
        }

        if (z2 < 0.0f || z2 > 1.0f)
            return {true, t1, t1};

        return {true, t1, t2};
    }

    std::tuple<bool, float, float> ray_cyllinder_intersection(const ray &r, const cyllinder &c) {
        return ray_cyllinder_intersection(r, c, intersection_cache<cyllinder>{c});
    }

    std::tuple<bool, float, float> ray_capsule_intersection(const ray &r, const capsule &c, const intersection_cache<capsule> &cache) {
        bool s1intersect, s2intersect, cyllinderIntersect;
        float s1t1, s1t2, s2t1, s2t2, ct1, ct2;
        std::tie(s1intersect, s1t1, s1t2) = ray_sphere_intersection(r, {c.start, c.radius});
        std::tie(s2intersect, s2t1, s2t2) = ray_sphere_intersection(r, {c.end, c.radius});
        std::tie(cyllinderIntersect, ct1, ct2) = ray_cyllinder_intersection(r, {c.start, c.end, c.radius}, cache.cyllinder_cache);

        if (s1intersect || s2intersect || cyllinderIntersect) {

            std::set<float> t1, t2;

            if (s1intersect) {
                t1.insert(s1t1);
                t2.insert(s1t2);
            }
            if (s2intersect) {
                t1.insert(s2t1);
                t2.insert(s2t2);
            }
            if (cyllinderIntersect) {
                t1.insert(ct1);
                t2.insert(ct2);
            }

            return {true, *t1.begin(), *t2.rbegin()};

        } else {
            return {false, 0.0f, 0.0f};
        }

    }

    std::tuple<bool, float, float> ray_capsule_intersection(const ray &r, const capsule &c) {
        return ray_capsule_intersection(r, c, intersection_cache<capsule>(c));
    }

    std::tuple<bool, float> ray_flat_capsule_intersection(const vec3 &origin, const flat_capsule &c, const intersection_cache <flat_capsule> &cache) {

        auto lower_point = c.start;
        auto higher_point  = c.end;

        if (lower_point.y() > higher_point.y())
            std::swap(lower_point, higher_point);

        // Check lower circle intersection

        {
            vec2 diff{{origin.x() - lower_point.x(), origin.z() - lower_point.z()}};
            if (diff.length_squared() <= c.radius * c.radius) {
                if (origin.y() < lower_point.y())
                    return {true, lower_point.y() - origin.y()};
                else return {false, 0.0f};
            }
        }

        vec2 dir{{lower_point.x() - higher_point.x(), lower_point.z() - higher_point.z()}};

        bool intersecting;
        float t1, t2;
        std::tie(intersecting, t1, t2) = ray_circle_intersection_2d({maths::vec2{{origin.x(), origin.z()}}, dir},
                                                                    {maths::vec2{{lower_point.x(), lower_point.z()}}, c.radius});
        if(!intersecting)
            return {false, 0.0f};

        if ((t1 * dir).length_squared() > dir.length_squared())
            return {false, 0.0f};

        vec3 x = vec3{{origin.x(), lower_point.y(), origin.z()}};
        vec3 b = t1 * vec3{{dir.x(), 0.0f, dir.y()}};
        vec3 a = x + b;

        vec3 dir3d = lower_point - higher_point;
        dir3d.normalize();

        auto l = b.length_squared() / (dir3d * b);
        vec3 intersection = a - dir3d * l;

        if (origin.y() > intersection.y())
            return {false, 0.0f};

        return {true, intersection.y() - origin.y()};
    }

    std::tuple<bool, float> ray_flat_capsule_intersection(const vec3 &origin, const flat_capsule &c) {
        return ray_flat_capsule_intersection(origin, c, intersection_cache<flat_capsule>{c});
    };

    std::tuple<bool, float, float> ray_circle_intersection_2d(const ray2d &r, const circle2d &c, const intersection_cache<circle2d> &cache) {

        vec2 l = c.center - r.origin;

        auto A = r.direction.length_squared();
        auto B = -2.0f * (l * r.direction);
        auto C = l.length_squared() - c.radius * c.radius;

        auto delta = B * B - 4.0f * A * C;
        if(delta < 0.0f)
            return {false, 0.0f, 0.0f};

        auto deltaSqrt = static_cast<float>(std::sqrt(delta));

        auto aInv = 0.5f / A;

        auto t1 = (-B - deltaSqrt) * aInv;
        auto t2 = (-B + deltaSqrt) * aInv;

        if (t1 > t2)
            std::swap(t1, t2);

        if (t1 < 0.0f)
            t1 = t2;

        if (t1 < 0.0f)
            return {false, 0.0f, 0.0f};

        return {true, t1, t2};
    }

    std::tuple<bool, float, float> ray_circle_intersection_2d(const ray2d &r, const circle2d &c) {
        return ray_circle_intersection_2d(r, c, intersection_cache<circle2d>(c));
    }

}