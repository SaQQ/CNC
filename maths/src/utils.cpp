// Created by Paweł Sobótka on 26.10.17.
// Rail-Mil Computers Sp. z o.o.

#include "utils.hpp"

namespace maths {

    vec3 orthogonal_vector(const vec3 &v) {
        auto x = std::abs(v.x());
        auto y = std::abs(v.y());
        auto z = std::abs(v.z());

        auto other = x < y ? (x < z ? vec3{{1.0f, 0.0f, 0.0f}} : vec3{{0.0f, 0.0f, 1.0f}}) : (y < z ? vec3{{0.0f, 1.0f, 0.0f}} : vec3{{0.0f, 0.0f, 1.0f}});

        return cross(other, v);
    }

}