cmake_minimum_required(VERSION 3.6)
project(marching_cubes)

file(GLOB_RECURSE HEADERS "include/*.hpp")
file(GLOB_RECURSE SOURCE_FILES "src/*.[hc]pp")

add_library(${PROJECT_NAME} ${SOURCE_FILES} ${HEADERS})

target_include_directories(${PROJECT_NAME} PUBLIC include)

target_compile_features(
        ${PROJECT_NAME}
        PUBLIC
)

target_link_libraries(${PROJECT_NAME} maths)