// Created by Paweł Sobótka on 16.08.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef MARCHING_CUBES_MARCHING_CUBES_HPP
#define MARCHING_CUBES_MARCHING_CUBES_HPP

#include <vector>
#include <utility>

#include <matrix.hpp>

namespace marching_cubes {

    using field_type = std::vector<std::vector<std::vector<float>>>;

    struct vertex {
        maths::vec3 pos;
        maths::vec3 normal;
    };

    std::pair<std::vector<unsigned int>, std::vector<vertex>>
    generate_surface(const std::vector<std::vector<std::vector<float>>> &field, float iso, maths::vec3 unit,
                     std::size_t min_x, std::size_t max_x, std::size_t min_y, std::size_t max_y, std::size_t min_z, std::size_t max_z);

}

#endif //MARCHING_CUBES_MARCHING_CUBES_HPP
