cmake_minimum_required(VERSION 3.6)
project(cnc_simulator)

set(CMAKE_CXX_STANDARD 17)

add_subdirectory(maths)
add_subdirectory(marching_cubes)
add_subdirectory(app)